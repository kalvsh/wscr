﻿WSCR roadmap

#######################################################################
how to read/write this document

In implementation of features,  four types of SUB-TASKS are predicted:
 → define/determine
 → discover
 → create & implement
 → fix
 → feature request
  

code for status (state of implementation process):
 status: ready/solved/ok 
 status: todo/under construction 
 status: info 
 status: broken/
#######################################################################


TASK1 31 OUT 2021
---------------------------------------------------------------------
feature request  - STATUS todo

- in the Paragraphs table of {PRJNAME}_texts.sqlite db scrapes the 
records for the column paragraph should be included WITHOUT html 
paragraph marks (i.e., strip these off <p> ... </p>)
---------------------------------------------------------------------


