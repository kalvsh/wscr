'''
hand made crawler for wbz purposes with cleaner component
 - set up
 - build index
 - visit, clean and save pages
'''
import os
import re
import sys
import logging
import argparse

from wscr import cfg 
from wscr.app import fio

from wscr.app.crawler import crawl_dispatcher
from wscr.app.creator import create_project
from wscr.app.scraper import scrape_pages

__author__  = 'kalvsh'
__version__ = '0.3.3' #21 OCT 2021

logging.basicConfig(filename=cfg.LOG, 
                    filemode = 'w', 
                    force = True, 
                    #encoding='utf-8', 
                    level=logging.DEBUG)

ARGS = 'create crawl scrape'

MSG1 = 'project name should not end in or contain substring .yaml'
MSG2 = 'bad mode parameter. Got %s, expected one of these: %s'

HELP1 = "choose one of these: %s"%ARGS
HELP2 = "type a project spec file name"

def run_cli(projects = cfg.PROJECTS, texts = cfg.TEXTS, args = None):
    if not args:
        args = get_parser(sys.argv[1:])

    if '.yaml' in args.project:     
        raise Exception(MSG1)
    
    if args.mode not in ARGS.split():
        raise Exception( MSG2 %(args.mode, ARGS) )
 
    index_pth = os.path.join(projects, args.project + '.sqlite') 
    spec_pth  = os.path.join(projects,  args.project + '.yaml')

    txts_pth = os.path.join(texts, args.project + '_texts.sqlite' ) 
    clnr_pth  = os.path.join(texts,  args.project + '_clean.yaml')

    logging.info('Starting wscr in mode *%s*' %args.mode)
    logging.info(' Starting process to %s project: %s' %(args.mode, args.project))

    if args.mode == 'crawl':
        crawl_dispatcher(spec_pth, index_pth)

    elif args.mode == 'create':
        create_project(spec_pth, index_pth, clnr_pth, txts_pth)

    elif args.mode == 'scrape':
        scrape_pages(index_pth, clnr_pth, txts_pth)

def get_parser(arglist):
    parser = argparse.ArgumentParser()
    parser.add_argument("mode",  
                        action='store',
                        choices=('create', 'crawl', 'scrape'),
                        help= HELP1,
                        type=str)
    parser.add_argument("project",  
                        action='store', 
                        help=HELP2,
                        type=str)
    return parser.parse_args(arglist)
