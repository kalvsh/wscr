'''
module with functions to crud a crawl index sqlite3 file
'''
import os
import sqlite3
import logging
import datetime as dt

from wscr.app import schema

msg1 = 'name of sqlite db index should end with .sqlite extension'
msg2 = 'sqlite3 db already exists'

msg3 =  'path argument should end in .sqlite ext!'

msg4a = 'the index you are trying to create does not exist\n'
msg4b = 'use the create mode in the CLI to create one'

def create_db(dbfpth, tables = schema.CREATE_INDEX):
    '''
    creates sqlite index db
    
    HOW IT WORKS: trivial

    PARAMETERS
     :dbfpth --> db file path
    '''
    if not dbfpth.endswith('.sqlite'):
        raise Exception(msg1)
    if os.path.exists(dbfpth):
        raise Exception(msg2)
    conn = sqlite3.connect(dbfpth)
    c = conn.cursor()
    for t in tables:
        c.execute(tables[t])
    return True

class DBManager():
    def __init__(self, path):

        if not path.endswith('.sqlite'):
            raise Exception(msg3)

        if not os.path.exists(path):
            raise Exception(msg4a+msg4b)

        self.path = path
        self.conn = None
    
    def get_conn(self):
        self.conn = sqlite3.connect(self.path)
        return self.conn

class DBAccess():
    def __init__(self, dbf_path):
        '''
        :limit boolean; is there a limit to the number of pages to crawl?
        :maxi int; max number of pages to crawl (if there is a limit)
        '''
        self.con = DBManager(dbf_path).get_conn()

        #should be human readable! 
        self.start = dt.datetime.now()
        self.end = False

        #self.crawlid = False

    def do_insert(self, statement, values = ''):
        try:
            with self.con:

                if not values:
                    ins = self.con.execute(statement)
                else:
                    ins = self.con.execute(statement, values)

                vid = ins.lastrowid

        except sqlite3.IntegrityError as e:
                logging.warning(e)
                return False

        return vid

    def do_select_all(self, statement, values = ''):
        try:
            with self.con:

                if not values:
                    sel = self.con.execute(statement)
                else:
                    sel = self.con.execute(statement, values)

                rows = sel.fetchall()

        except sqlite3.IntegrityError as e:
                logging.warning(e)
                return False

        return rows
