from wscr.app import fio
from wscr.app import dbmgr
from wscr.app import schema
from wscr.app import cleaners

def scrape_pages(idxdb_pth, clnr_guide_pth, txtdb_pth):
    scrpr = Scraper(idxdb_pth)
    indexed_pages = scrpr.get_pages()

    txtdb_writer = TextWriter(txtdb_pth)

    tags = get_tags(clnr_guide_pth)

    cleaner = cleaners.Cleaner(tags['start_tag'], tags['end_tag'], tags['keep_paragraph'])
    txtdb_writer.set_cleaner(cleaner)

    ins_page = schema.INSERT_TEXT_DB['Pages']
    ins_paragraph = schema.INSERT_TEXT_DB['Paragraphs']

    for row in indexed_pages:

        urlid, url, markup, title, crawl_start = row

        #print(urlid, url)

        paragraphs = cleaner.get_text_from_p_tag(markup)

        #print(paragraphs)

        pgid = txtdb_writer.do_insert( ins_page, [url, title, urlid, crawl_start] )
        #print(pgid, '#\t', url)
        
        if not pgid: continue #warn user pg has not been updated
        
        for n, paragraph in enumerate(paragraphs):
            #try: print (n, paragraph[:9])
            #except: print('%$#@&!'*8)
            txtdb_writer.do_insert( ins_paragraph, [n + 1, paragraph, pgid ] )

def get_tags(clnr_guide_pth):
    cg = fio.read_yaml(clnr_guide_pth)

    for key, tag in cg.items():
        if 'tag' not in key: continue
        else:
            if type(tag) is not bytes:
                cg[key] = bytes(tag, 'utf8')
    return cg

class Scraper(dbmgr.DBAccess):

    def get_pages(self):
        cmd = schema.SELECT_INDEX['all_pages']
        self.records = self.do_select_all(cmd)
        self.close()
 
        return self.records

    def close(self):
        self.con.close()

class TextWriter(dbmgr.DBAccess): 

    def set_cleaner(self, cleaner):
        self.cleaner = cleaner
