'''
index maker for crawl projects
'''
import os
import sqlite3
import logging
import datetime as dt

from wscr import cfg
from wscr.app import dbmgr
from wscr.app import schema

DIGITS = '0 1 2 3 4 5 6 7 8 9'.split()
LOWERCASE = 'a b c d e f g h i j k l m n o p q r s t u v w x y z'.split()
UPPERCASE =  [char.upper() for char in LOWERCASE]

class Indexer(dbmgr.DBAccess):

    def open(self, project, strategy, limit, max_pages):

        #create a code for this crawl
        #NB including project name is important. 
        #It is used by webapp to help trace back sentences to their sources.
        self.code = self.generate_code(project)

        if limit: mp = max_pages
        else: mp = False

        self.crawlid = self.do_insert(schema.INSERT_INDEX['Crawls'], 
                                     (self.code, self.start, mp, strategy))

        self.con.commit()

    def generate_code(self, project):
        """ Return base 36 representation for int (self.start) """
        base_n_digits = DIGITS + LOWERCASE + UPPERCASE
        result = ""
        n =  int(self.start.strftime('%s') )
        while n > 0:
            q, r = divmod(n, 36)
            result += base_n_digits[r]
            n = q
        if result == "":
            result = "0"
        b36 =  ''.join(reversed(result))

        return '%s:%s'%(project, b36)

    def close(self):
        self.end = (dt.datetime.now())
        
        self.do_insert(schema.UPDATE_INDEX['Crawls'], (self.end, self.crawlid))

        self.con.close()
 
    def index_url_visit(self, url, data, status, title ):
        if title and status == 200 or status == '200 OK':
            self.index_page(url, data, title)
        else:
            self.index_register_bad_url(url, status)

    def index_register_bad_url(self, url, status):
        return self.do_insert(schema.INSERT_INDEX['BadURLs'],
                              (url, status, self.crawlid))

    def index_page(self, url, html, title):
        urlid = self.do_insert(schema.INSERT_INDEX['URLs'], (url, self.crawlid))

        if not urlid:
            urlid = mupid = insid = False
        else:
            mupid = self.do_insert(schema.INSERT_INDEX['Markups'], (html, urlid))
            insid = self.do_insert(schema.INSERT_INDEX['Titles'], (title, urlid, mupid))
 
        return urlid and mupid and insid

    def get_indexed_pages(self):
        return self.do_select_all( schema.SELECT_TEXT_DB['all_pages'] )
