# -*- encoding: utf8 -*-
''' 
functions to extract nl text from html files (clean up html, js, css
and anything which is not nl text)

 --> _cleanup functions connect to crawl projects and call cleaner functs
 --> _trim and _clean functions do actual wiping out
'''
import re
from bs4 import BeautifulSoup

from wscr.app import tasks

class Cleaner:
    def __init__(self, start_tag, end_tag, keep_p):
        self.ept1 = start_tag
        self.ept2 = end_tag
        self.keep_p = keep_p

    def get_text_from_p_tag(self, markup):
        '''
        returns nl text found in markup as string

        HOW IT WORKS
        sends markup and endpoints to str_trim
        makes returned string a BS2 soup obj
        extracts text found soup's html <p> 

        PARAMETERS
        :markup --> str, html markup
        :ept1   --> str, endpoint 1
        :ept2   --> str, endpoint 2
        :keep_p4 --> boolean
        '''

        trimmed = str_trim(markup, self.ept1, self.ept2)
        s = tasks.make_soup(trimmed)
        text = []

        #get all p tags from markup
        for tag in s('p'):

            #make them into nl paragraphs
            paragraph = tag.get_text()
            paragraph = str_clean(paragraph)

            #print (paragraph)
            if len(paragraph) == 0:
                continue
            if len(paragraph) < 2 and paragraph[-1].isalnum():
                continue

            if self.keep_p:
                paragraph = "<p>" + paragraph + "</p>"
        
            text.append(paragraph)
        return text
        #return ' '.join(text)

def clean_by_tags(markups, project, keep_p):
    '''
    return list of str (nl texts found in markups)

    HOW IT WORKS
     gets endpoint tags for trimming html from project
     sends each markup to be cleaned by str_cleanup_by_tags

    PARAMETERS
     :markups --> list of strings
     :project --> a crawl project object
     :keep_p  --> boolean
    '''
    ept1 = project.parse_guide.start_tag
    ept2 = project.parse_guide.end_tag
    puncts = '. ! ? _"'#what is this doing here???
    return [get_text_from_p_tag(mup, ept1, ept2, keep_p) for mup in markups]

def get_text_from_p_tag(markup, ept1, ept2, keep_p):
    '''
    returns nl text found in markup as string

    HOW IT WORKS
     sends markup and endpoints to str_trim
     makes returned string a BS2 soup obj
     extracts text found soup's html <p> 

    PARAMETERS
     :markup --> str, html markup
     :ept1   --> str, endpoint 1
     :ept2   --> str, endpoint 2
     :keep_p4 --> boolean
    '''

    trimmed = str_trim(markup, ept1, ept2)
    s = tasks.make_soup(trimmed)

    #get all p tags from markup
    for tag in s('p'):

        #make them into nl paragraphs
        paragraph = tag.get_text()
        paragraph = str_clean(paragraph)

        #print (paragraph)
        if len(paragraph) == 0:
            continue
        if len(paragraph) < 2 and paragraph[-1].isalnum():
            continue

        if keep_p:
            paragraph = "<p>" + paragraph + "</p>"
        
        text.append(paragraph)
    return ' '.join(text)

#===============================================================================
# functions for trimming
#===============================================================================
def str_trim(html, mark1 = False, mark2 = False):
    '''
    returns html[mark1:mark2]

    ATTENTION: no safety nets against problems in indexes
    for example mark1 > mark2

    PARAMETERS
     :html  --> str
     :mark1 --> int
     :mark2 --> int
    '''
    #indices are endpoints of html str
    i1 = 0
    i2 = len(html)
    #reassign limits if possible
    if mark1:
        #print before, html
        x = html.find(mark1)
        if x != -1: i1 = x
    if mark2:
        x = html.rfind(mark2)
        if x != -1: i2 = x
    return html[i1:i2]

def find_element(soup, tag, attributes):
    '''convenience method to find tag with bs4'''
    return soup.findAll(tag, attrs=attributes)

#trimmer name convention: 'name of corpus' + 'trim'

#===============================================================================
# cleaning
#===============================================================================
def str_clean(html):
    '''
    returns str

    PARAMETERS:
     :html --> str
    '''
    page = re.sub('\s+', ' ', html)
    page = re.sub('<b>.*</b>','' , page, 1)
    page = re.sub('<script .+?>.*</script>', '',page )
    page = re.sub('<.*?>','', page)
    page = re.sub('&nbsp;(.*?)\s\s','', page)
    page = re.sub('\(adsbygoogle.*;', '', page)
    page = re.sub('var(.+?);','', page)
    page = re.sub('\s+', ' ', page)
    page = re.sub('\\n+', ' ', page)
    page = re.sub("\\\\'", "'", page)
 
 
    return page
