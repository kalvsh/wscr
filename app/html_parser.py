import bs4

from wscr.app import cleaners
from wscr.app.islink import LinkValidator

class HTMLParser:
    def __init__ (self, rules):
       self.validator = LinkValidator(rules)

    def make_soup(self, html):
        self.soup = bs4.BeautifulSoup(html, 'html.parser')
 
    def find_url_links(self):
        good_links = set()

        for link in self.soup.findAll('a'):

            if 'href' not in link.attrs: continue

            href =  link.attrs['href'] 

            if self.validator.is_good_link(href): good_links.add(href)
            else: continue

        return good_links

    def find_title(self):
        return self.soup.findAll('title')

    def get_page_title(self):
        '''
        returns page title as str (for sql insert statement)
        '''
        t = self.find_title()
        if not t:
            t = ''
        else:
            t = cleaners.str_clean(str(t))

        return t
