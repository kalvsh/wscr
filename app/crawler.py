import os
import sys
from collections import deque

import bs4

#local = os.path.split(os.path.abspath(__file__))[0]
#mother = os.path.split(local)[0]
#sys.path.append(mother)
#sys.path.append(local)

from wscr import cfg
from wscr.app import fio
from wscr.app import indexer
from wscr.app import pagevisitor
from wscr.app import html_parser
from app.validator import is_crawlable

def crawl_dispatcher(spec_pth, index_pth):

    if not os.path.isfile(index_pth):
        msg = 'cannot crawl; index file \n%s\n does not exist'%(index_pth)
        raise Exception(msg)

    if not os.path.isfile(spec_pth):
        msg = 'cannot crawl; spec file \n%s\n does not exist'%(spec_pth)
        raise Exception(msg)

    spec = fio.read_yaml(spec_pth)
    validator = fio.read_yaml(cfg.SPEC_VAL_PTH)

    if is_crawlable(spec, validator['model'], validator['field_types']):

        if spec['strategy'] == 'link_crawl':
            seed_urls = spec['link_crawl']['seeds']
        elif spec['strategy'] == 'archive_crawl':
            seed_urls = crawl_listing(spec, spec['strategy'])
        elif spec['strategy'] == 'search_crawl':
            seed_urls  = get_search_results(spec, spec['strategy'])
        else:
            msg = 'unknown crawl strategy: %s'%spec['strategy']
            raise Exception(msg)

        idx = indexer.Indexer(index_pth)
        
        pgv = pagevisitor.PageVisitor()

        prs = html_parser.HTMLParser(spec['rules'])

        pgc = PageCrawler(spec, idx, pgv, prs, seed_urls)
        pgc.start()

class PageCrawler:
    def __init__(self, spec, index, pgvisitor, parser, seed_urls):
        self.spec = spec

        self.project = spec['project']
        self.limit = spec['limit']
        self.max = spec['max']
        self.strategy = spec['strategy']

        self.index = index
        self.parser = parser
        self.pgvisitor = pgvisitor

        self.urls = deque(seed_urls)
        self.indexed = []
        self.bounced = []
        self.visit_count = 0

    def start(self):
        #register crawl: call index function to register crawl
        self.index.open(self.project, self.strategy, self.limit, self.max)

        #get indexed pages if any
        self.indexed = self.index.do_select_all('SELECT * FROM URLs')
        
        #start actual crawl
        self.crawl_pages()

        self.index.close()
        
    def crawl_pages(self):
        #self.urls is a collection.deque
        while self.urls:

            url = self.urls.popleft()

            if self.limit and self.visit_count >= self.max:  break

            elif url in self.indexed: continue

            elif url in self.bounced: continue
          
            else: 
                
                response = self.pgvisitor.visit(url)
                self.visit_count += 1
                
                if response.status == 200 or response.status == '200 OK':

                    self.parser.make_soup(response.data)
                    new_links = self.parser.find_url_links()

                    for nl in new_links:
                        if nl not in self.urls: 
                            self.urls.append(nl)

                    title = self.parser.get_page_title()

                    self.indexed.append(url)
                
                else:
                    self.bounced.append(url)
                    title = False

                self.index.index_url_visit(url, response.data, response.status, title)

        return True

def crawl_listing(spec, strategy):
    #for url in spec[strategy]['listing']: pass
    raise Exception('function crawl listing not implemented!')
        
def get_search_results(spec, strategy):
    #spec['search_crawl']['listing'] = do_search(spec['search_crawl']['terms'])
    #crawl_listing(spec, strategy)
    raise Exception('function get_search_results not implemented')

def do_search(terms):
    raise Exception('function do_search not implemented')
