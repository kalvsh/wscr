import os
import re

MSG1 = '%s %s has unexpected type %s. Should be string'

class LinkValidator:
    def __init__(self, rules):
        self.rules = rules

    def is_good_link(self, link):
        if ' ' in link: return False

        if is_absolute_link(link):
            if link.startswith('http://'):
                link = link[7:]
            elif link.startswith('https://'):
                link = link[8:]

        validation = self.apply_rules(link)
        accept = validation[0]
        reject = validation[1]

        return accept and not reject

    def apply_rules(self, link): 
        accept = False
        reject = False
            
        for boolean in self.validate(link, self.rules['accept']):
            if boolean: accept = True

        for boolean in self.validate(link, self.rules['reject']):
            if boolean: reject = True
                
        return accept, reject

    def validate(self, link, subrules):
        prefix = False
        infix = False
        suffix = False
        regex = False

        for field in subrules:
            values = subrules[field]

            if field == 'prefix':
                for value in values:
                    if has_prefix(link, value): prefix = True

            elif field == 'infix':
                for value in values:
                    if has_infix(link, value): infix = True

            elif field == 'suffix':
                for value in values:
                    if has_suffix(link, value): suffix = True

            elif field == 'regex':
                for value in values:
                    if has_regex(link, value): regex = True

        return prefix, infix, suffix, regex

def is_absolute_link(link):
    schemes = 'http:// https://'.split()
    for scheme in schemes:
        if link.startswith(scheme): return True
    return False

def has_prefix(link, prefix):
    ltype = type(link) == type('') 
    ptype = type(prefix) == type('') 

    if not ltype: raise Exception(MSG1%('link', link, type(link)))
    if not ptype: raise Exception(MSG1%('prefix', prefix, type(prefix)))
 
    if prefix == '': return False
    return link.startswith(prefix)

def has_infix(link, infix):
    ltype = type(link) == type('') 
    itype = type(infix) == type('') 

    if not ltype: raise Exception(MSG1%('link', link, type(link)))
    if not itype: raise Exception(MSG1%('infix', infix, type(infix)))
 
    if infix == '': return False
    return infix in link and not link.startswith(infix) and not link.endswith(infix)

def has_suffix(link, suffix):
    ltype = type(link) == type('') 
    stype = type(suffix) == type('') 

    if not ltype: raise Exception(MSG1%('link', link, type(link)))
    if not stype: raise Exception(MSG1%('suffix', suffix, type(suffix)))
 
    if suffix == '': return False
    return link.endswith(suffix)

def has_regex(link, regex):
    ltype = type(link) == type('') 
    rtype = type(regex) == type('') 

    if not ltype: raise Exception(MSG1%('link', link, type(link)))
    if not rtype: raise Exception(MSG1%('suffix', regex, type(regex)))
    
    return re.search(regex, link)
               
def calculate_truth_value(booleans, connector):
        if len(booleans) == 0:
            return None
        elif connector == 'or':
            if True in booleans: return True
            else: return False
        elif connector == 'and':
            if False in booleans: return False
            else: return True
