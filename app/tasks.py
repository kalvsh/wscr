'''
utility functions for the wbz-crawl package
'''
import codecs
import os, urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup as bs

def get_html(url):
    '''
    returns string
    
    HOW IT WORKS
    connects to url with urllib2.urlopen 
    returns markup if response is 200
    if not returns 0

    PARAMETERS
     :url --> url of www page
    '''
    response = urllib.request.urlopen(url)
    ct   = response.getheader('Content-Type')
    code = response.code
    if 'text/html' in ct and code == 200:
        return str(response.read())
    else: return 0

def make_soup(html):
    '''
    returns bs4 object

    HOW IT WORKS: trivial

    PARAMETERS
     :html --> str of html markup
    '''
    if html == 0: return None
    return bs(html, 'lxml')

def get_hrefs(tags, accept, refuse):
    '''
    returns list of strings:
    hrefs attrs of <a> elements in html for page at :param url

    HOW IT WORKS: trivial

    PARAMETERS
     :tags --> list of bs4.element.Tag objects
     :accept --> dict of tags and substrings(xor) that are accepted values
                  for them
     :refuse --> dict of tags and substrings(xor) that are  NOT accepted 
                  values for them

    '''
    tags  = have_attr_value(tags, 'href', accept)
    tags  = no_have_attr_value(tags, 'href', refuse)
    nice_tags =  [tag.attrs['href'] for tag in tags]
    return nice_tags

def have_attr_value(elements, attr, restr):
    '''
    returns list of elements (BeautifulSoup.Tag objects) whose value
    for attribute attr are allowed by restr

    HOW IT WORKS
     delegates checking for attribute values to has_substr function
     
 
    PARAMETERS
     :elements --> list of BeautifulSoup.Tag objects with attrs attribute
                present

     :attr  --> str, value of attrs attribute in each e in elements

     :restr --> dict with substrings (xor) that should be found in
                value of attrs {'in':[...], ''}
    '''
    #return [e.attrs[attr] for e in elements if has_substr(e.attrs[attr,restr])]
    chosen = []
    for e in elements:
        if attr in e.attrs:
            if  has_substr(e.attrs[attr], restr):
                chosen.append(e)#.attrs[attr])
        else: continue
    return chosen

def no_have_attr_value(elements, attr, restr):
    '''
    returns list of elements whose value for attribute attr are allowed
    by restr

    HOW IT WORKS

    PARAMETERS
     :elements:  list of BeautifulSoup.Tag objects with attrs attribute
                present

     :attr:      str, value of attrs attribute in each e in elements

     :restr:     dict with substrings (xor) that should NOT be found in
                value of attrs
    '''

    #return [e.attrs[attr] for e in elements 
                #if not has_substr(e.attrs[attr,restr])]
    chosen = []
    for e in elements:
        if attr in e.attrs:
            if not has_substr(e.attrs[attr], restr):
                chosen.append(e)
        else:continue
    return chosen

def write_repo(texts, urls, prj_repo, idx_db):
    '''
    writes each text as a file in project repo dir 

    file name is url of page where text is taken from
    
    HOW IT WORKS: see line comments at code

    PARAMETERS
     :texts --> list of strs (texts found in webpages)
     :urls  --> list of strs (urls where texts came from)
     :prj_repo --> path to repo for project
     :idx_db   --> path to prj index sqlite db file
    '''
    #cursor   = dbmgr.conn_urldb(idx_db)
    #dbrecs   = dbmgr.get_index(cursor)
    old_files = os.listdir(prj_repo)#[rec[0] for rec in dbrecs]
    for t,u in zip(texts, urls):

        #trim url to become a txt file name 
        if u.startswith('/'): u = u[1:]
        if u.startswith('https://'): u[8:]
        if u.startswith('http://'): u[7:]
        if u.endswith('/'): u = u[:-1]
 
        if '/' in u:
            u = u.replace('/', '_')
        bare_url  = os.path.splitext(u)
        bare_name = bare_url[0]
        txtf_name = bare_name + '.txt'

        if txtf_name not in old_files:

            #create txt file in prj repo and save contents
            fadd = os.path.join(prj_repo, txtf_name)

            #from SO recipe...
            fh = open(fadd, 'w', encoding='utf-8')
            fh.write(t)
            fh.close

            #append to new_urls and later write to index
            #new_urls.append(u)
    #dbmgr.update_index(new_urls, cursor)

def has_substr(candidate, restrictions, mode='include', condition='or'):
    '''
    returns bool according to if candidate has desired substrings or not
    
    HOW IT WORKS
    analyses if candidate meets requirements under conditions
    'or' (one requirement suffices) or 'and' (all requirements
    must be met)

    PARAMETERS
     :candidate  --> str
     :restrictions --> {'start':None,'in':None, 'end':None}
     :conditions --> or and
     :mode       --> include or refuse 
    '''
    score = []
    if restrictions['start'] != None and restrictions['start']!= [''] and restrictions['start'] != '':        
        marks = [candidate.startswith( _ ) for _ in restrictions['start']]
 
        if True in marks: score.append( True )
        else: score.append( False )
    else: 
        if mode == 'include': score.append(True)
        else: score.append(False)

    if restrictions['in'] != None and restrictions['in']!= [''] and restrictions['in'] != '':        

        marks = [ _ in candidate for _ in restrictions['in'] ]

 
        if True in marks: score.append( True )
        else: score.append( False )
    else: 
        if mode == 'include': score.append(True)
        else: score.append(False)

    if restrictions['end'] != None and restrictions['end']!= [''] and restrictions['end'] != '':

        marks = [ candidate.endswith( _ ) for _ in restrictions['end'] ]
        if True in marks: score.append( True )
        else: score.append( False )

    else:

        if mode == 'include': score.append(True)
        else: score.append(False)

    if condition == 'or':
        for s in score:
            if s is True :
                return True
        return False

    else:
        for s in score:
            if s is False:
                return False
        return True

def get_max_index(hrefs):
    '''
    expects a list of hrefs which end in digits (...bc they number smthg)
    returns the highest (max()) of the digits

    HOW IT WORKS

    PARAMETERS
     :hrefs --> str 
    '''
    numbers = []
    for href in hrefs:
            tail = os.path.split(href)[1]
            if tail.isdigit(): numbers.append( int(tail) )
    try: highest = max(numbers)
    except ValueError: highest = 0
    if highest == 0:
        #never return 0 there if the site is being
        #scraped there should be at least one archive page
        #so even in the case of error here it should work
        highest = 1
    return highest
 
def is_limit(limit, max_num):
    '''
    very simple helper for index_archive and index_content functions

    '''
    if limit:
        if  limit <= max_num:
           return limit
        else:
            #limit greater than max_num
            #return 0 to fail silently
            return 0
    else: return max_num

def make_soup2(url, trim = False, start_tag = None, end_tag = None):
    '''
    !!!!!!!!!!! BROKEN !!!!!!!!!!!

    returns page at :url as BeautifulSoup object

    HOW IT WORKS

    PARAMETERS
     :url - string
    '''
    def urlopen(url):
        response = urllib.request.urlopen(url)
        ct   = response.getheader('Content-Type')
        code = response.code
        if 'text/html' in ct and code == 200:
            return response.read()
        else: return 0

    def str_trim(html, before, after):
        '''
        !!!!!!!! FOUND THIS BROKEN !!!!!!!!
        parameters :before and :after are referred to as
        :before_this_target and :after_this_target in function body
        -----> is this legacy on its way to being deprecated?
        -----> or is it intended as a still unfinished replacement 
               for make_soup?
        '''
        #indices are endpoints of html str
        i1 = 0
        i2 = len(html)
        #reassign limits if possible
        if before:
            x = html.find(before_this_target)
            if x != -1: i1 = x
        if after:
            x = html.rfind(after_this_target)
            if x != -1: i2 = x
        return html[i1:i2]

    #make soup
    html = urlopen(url)
    if trim:
        html = str_trim(html, start_tag, end_tag)
    if html == 0: return None
    return bs(html, 'lxml')
