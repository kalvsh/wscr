import os
import sys

MSG1 = 'error validating list of spec fields!'
MSG2 = '%s field %s is missing from project spec'
MSG3 = '%s spec has field %s: it is not a valid field!'

def is_crawlable(spec,model, types):
    valid = is_valid(spec, model, types) 
    ready = spec['status'] == True

    if not valid:
        msg = 'project spec is not valid; please review it.'
        raise Exception(msg)
    elif not ready:
        msg = 'spec has status 0 (not ready for crawl); please review it.'
        raise Exception(msg)
    else:
        return valid and ready

def is_valid(spec, model, types):
    return validate(spec, model, types)

def validate(spec, model, types):
        '''
        returns True if values for keys in project spec match 
        counterparts in validator

        TODO report which keys caused non validation 
        '''
        valid = False
        matched_list, msg = is_matched_list(model.keys(), spec.keys())

        if matched_list:

            for k in model.keys():

                if type(model[k]) is dict:
                
                    valid = is_valid(spec[k], model[k], types)

                elif type(model[k]) is not dict : 
 
                    valid = type(spec[k]) == type(types[k])

                if not valid: 
                    msg = 'field %s has value %s. Type %s, expected %s'%(k, 
                            spec[k],type(spec[k]), type(types[k]))
                    raise Exception(msg)

        else: 
            raise Exception(msg)
 
        return valid

def is_matched_list(model_fields, spec_fields):
    for el  in model_fields:
        if not el in spec_fields:
            return False, MSG2 %(MSG1, el)
    for el in spec_fields:
        if not el in model_fields:
            return False, MSG3 %(MSG1, el)
    return True, ''
