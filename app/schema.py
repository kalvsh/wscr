import os

from wcorp import cfg
from wcorp.app import fio

CREATE_INDEX = fio.read_yaml( os.path.join(cfg.PROJECTS, 'sql_create.yaml') )
INSERT_INDEX = fio.read_yaml( os.path.join(cfg.PROJECTS, 'sql_insert.yaml') )
SELECT_INDEX = fio.read_yaml( os.path.join(cfg.PROJECTS, 'sql_select.yaml') )
UPDATE_INDEX = fio.read_yaml( os.path.join(cfg.PROJECTS, 'sql_update.yaml') )

CREATE_TEXT_DB = fio.read_yaml( os.path.join(cfg.TEXTS, 'sql_create.yaml') )
INSERT_TEXT_DB = fio.read_yaml( os.path.join(cfg.TEXTS, 'sql_insert.yaml') )
SELECT_TEXT_DB = fio.read_yaml( os.path.join(cfg.TEXTS, 'sql_select.yaml') )
UPDATE_TEXT_DB = fio.read_yaml( os.path.join(cfg.TEXTS, 'sql_update.yaml') )
