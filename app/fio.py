# -*- encoding: utf8 -*-
'''
Created on Sep 9, 2016 as utils.py
6 JAN 2018 as fio.py
11 NOV 2021 deleted some functions

utils for file io

@author: claud29
'''
import os, json, yaml
def read_annotation(pth):
    '''
    returns annotation dictionary
    '''
    fh = open(pth)
    return json.load(fh)

def read_csv_files(filenames, folder):
    '''
    returns dict where keys are filenames and values the contents
    of the respective files
    @param filenames :list of strings
    @param folder: str -> a path
    '''
    file_contents = {}
    for fn in filenames:
        pth = os.path.join(folder, fn)
        file_contents[fn] = read_csv_file(pth)
    return file_contents

def read_csv_file(fpath, recipient = []):
    '''
    returns recipient (list) with  file contents
    '''
    with open(fpath) as f:
        f_csv = f.readlines()
        for row in f_csv:
            if '#start' in row or '#end' in row: continue
            else: recipient.append(row[:-1])
    return recipient

def read_yaml(pth):
    '''
    saves a few lines of code
    '''
    with open(pth) as f:
        data = yaml.safe_load(f)
    f.close()
    return data

def read_yaml_file(folder, fname):
    '''
    saves a few lines of code
    '''
    pth = os.path.join(folder, fname)
    with open(pth) as f:
        data = yaml.safe_load(f)
    f.close()
    return data

def write_yaml(pth, content):
    '''
    saves a few lines of code
    '''
    with open(pth, 'w') as f:
        yaml.dump(content, f)
    return True

def write_yaml_file(folder, fname, content):
    '''
    saves a few lines of code
    '''
    pth = os.path.join(folder, fname)
    with open(pth, 'w') as f:
        yaml.dump(content, f)
    return True
