Feature: Creating a Crawl Project
	A crawl project can be created from the command line

	Scenario: Creating a Project which is New
		Given a file spec does not exist for project
		When I run "python create dummy_project"
		Then a dummy_project.yaml file is created
		Then a dummy_project.sqlite is created
	
	Scenario: Creating a Project which already exists
		Given a file called dummy_project.yaml exist in the repository
		When I run "python create dummy_project"
		Then no dummy_project.yaml file is created
		Then no dummy_project.sqlite is created



