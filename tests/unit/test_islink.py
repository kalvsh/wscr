import io

import mock
import pytest
from contextlib import redirect_stdout, redirect_stderr

from wscr.app import islink
def test_is_absolute_link():
    absolutes = 'https://www.lalala.com http://lalala.com https://lalala'.split()

    relatives = '/lalala.com lelele.com/page1'.split()

    for el in absolutes: assert islink.is_absolute_link(el)

    for el in relatives: assert not islink.is_absolute_link(el)

def test_is_good_link_absolute_urls(monkeypatch):
    val_link = islink.LinkValidator({})

    apply_rules = mock.Mock(return_value = (True, False) ) 
    monkeypatch.setattr(val_link, 'apply_rules', apply_rules)

    absolutes = 'https://www.lalala.com http://lalala.com https://lalala'.split()

    relatives = 'lalala.com page2.html /page3.net repo/page4.php'.split()

    has_space =  ('https://www.lalala.com ',  'http//:lal ala.com', 'lalala. com')

    for el in absolutes: 
        if el.startswith('http://'):
            assert apply_rules.called_with(el[7:])
        if el.startswith('https://'):
            assert apply_rules.called_with(el[8:])
 
        assert val_link.is_good_link(el)

    for el in relatives: 
        assert apply_rules.called
        assert val_link.is_good_link(el)

    for el in has_space: assert not val_link.is_good_link(el)

def test_apply_rules_true_true(monkeypatch): 
    rules = {'accept': [True],
             'reject': [True]}

    val_link = islink.LinkValidator(rules)

    validate = lambda x, y: y  
    monkeypatch.setattr(val_link, 'validate', validate)

    xpct = val_link.apply_rules('lalala')
    assert xpct[0] == True and xpct[1] == True

def test_apply_rules_true_false(monkeypatch): 
    rules = {'accept': [True],
             'reject': [False]}

    val_link = islink.LinkValidator(rules)

    validate = lambda x, y: y  
    monkeypatch.setattr(val_link, 'validate', validate)

    xpct = val_link.apply_rules('lalala')
    assert xpct[0] == True and xpct[1] == False

def test_apply_rules_false_true(monkeypatch): 
    rules = {'accept': [False],
             'reject': [True]}

    val_link = islink.LinkValidator(rules)

    validate = lambda x, y: y  
    monkeypatch.setattr(val_link, 'validate', validate)

    xpct = val_link.apply_rules('lalala')
    assert xpct[0] == False and xpct[1] == True

def test_apply_rules_false_false(monkeypatch): 
    rules = {'accept': [False],
             'reject': [False]}

    val_link = islink.LinkValidator(rules)

    validate = lambda x, y: y  
    monkeypatch.setattr(val_link, 'validate', validate)

    xpct = val_link.apply_rules('lalala')
    assert xpct[0] == False and xpct[1] == False

def test_validate_all_fields_True(monkeypatch):
    val_link = islink.LinkValidator({})
    
    for _ in  'has_prefix has_infix has_suffix has_regex'.split(): 
        monkeypatch.setattr(islink, _ , lambda x, y: True)

    subrules = {
            'prefix': [None],
            'infix': [None],
            'suffix': [None],
            'regex': [None]
                }

    results = val_link.validate('lalala', subrules)
    
    for r in results:
        assert r == True

def test_validate_one_field_False(monkeypatch):
    val_link = islink.LinkValidator({})
    
    for _ in  'has_infix has_suffix has_regex'.split(): 
        monkeypatch.setattr(islink, _ , lambda x, y: True)
    monkeypatch.setattr(islink, 'has_prefix' , lambda x, y: False)

    subrules = {
            'prefix': [None],
            'infix': [None],
            'suffix': [None],
            'regex': [None]
                }

    results = val_link.validate('lalala', subrules)
    
    assert results[0] == False
    for r in results[1:]:
        assert r == True

def test_has_prefix():
    err = io.StringIO()
    assert not islink.has_prefix('', 'www')
    
    msg = islink.MSG1%('link', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_prefix(123, 'wwww')
    
    msg = islink.MSG1%('prefix', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_prefix('www', 123)

    assert islink.has_prefix('www.lalala.com/parte1', 'www.lalala.com')

def test_has_infix():
    err = io.StringIO()
    assert not islink.has_infix('', 'www')
    
    msg = islink.MSG1%('link', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_infix(123, 'wwww')
    
    msg = islink.MSG1%('infix', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_infix('www', 123)

    assert islink.has_infix('www.lalala.com/parte1/page1', 'parte1')
    assert not islink.has_infix('www.lalala.com/parte1', 'www.lalala.com')
    assert not islink.has_infix('www.lalala.com/parte1', 'www')

def test_has_suffix():
    err = io.StringIO()
    assert not islink.has_suffix('', 'www')
    
    msg = islink.MSG1%('link', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_suffix(123, 'wwww')
    
    msg = islink.MSG1%('suffix', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_suffix('www', 123)

    assert not islink.has_suffix('www.lalala.com/parte1/page1', 'parte1')
    assert not islink.has_suffix('www.lalala.com/parte1', 'www.lalala.com')
    assert  islink.has_suffix('www.lalala.com/parte1/page1', 'parte1/page1')

def test_has_regex():
    err = io.StringIO()
 
    msg = islink.MSG1%('link', 123, type(1))
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        islink.has_regex(123, 'wwww')

    assert islink.has_regex('www.lalala.com/parte1/page2', '/parte[1-3]')
    assert not islink.has_regex('www.lalala.com/parte4/page2', '/parte[1-3]')

