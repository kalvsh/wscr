import mock
import pytest

import bs4

from wscr.app import html_parser

#some utilities are in __init__
from wscr.tests.convenience_functions import SPEC_FNAME, MOCK_SPEC , MyObj

url1 = 'www.cip.com.br'
url2 = "https://www.w3schools.com/"
url3 = '/alunos/notas.html'
link1 = '<a href="%s">link text</a>'%url1
link2 = '<a href="%s">Visit W3S</a>'%url2
link3 = '<a href="%s">relative url</a>'%url3
not_link = '<a > not a link </a> '
html = '<html>%s %s %s  %s </html>'%(link1, link2, link3, not_link)

#######################################################################
#HMTLParser
#######################################################################
def test_find_url_links_returns_expected_href_links(create_mock_project, monkeypatch):
    prs = html_parser.HTMLParser(MOCK_SPEC)
    MyObj.is_good_link = lambda x, y: True
    prs.validator = MyObj()
    prs.soup = bs4.BeautifulSoup(html, 'html.parser')
    
    urls = prs.find_url_links()

    for u in [url1, url2, url3]: assert u in urls
    #not_link not returned
    assert len(urls) == 3
    assert type(urls) == type(set())

def test_find_url_links_bad_urls_returns_empty(create_mock_project, monkeypatch):
    prs = html_parser.HTMLParser(MOCK_SPEC)
    MyObj.is_good_link = lambda x, y: False
    prs.validator = MyObj()
    prs.soup = bs4.BeautifulSoup(html, 'html.parser')
    
    urls = prs.find_url_links()

    assert len(urls) == 0
    assert type(urls) == type(set())

@pytest.mark.skip(reason = 'decide if necessary, implement later')
def test_make_soup_expects_string():
    assert False

def test_get_page_title():
    assert False


