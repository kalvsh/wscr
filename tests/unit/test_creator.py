import os
import io
import yaml
import mock

from contextlib import redirect_stderr

import pytest

from wscr import cfg
from wscr.app import creator

from wscr.tests.convenience_functions import mk_spec, mk_sqlite_db, IDX_PTH, TEXTS_PTH
from wscr.tests.convenience_functions import TEXTS_FNAME, SPEC_FNAME, SPEC_PTH
VF_NAME = 'spec_validator.yaml'

PRJ_NAME = 'lalala'
SPEC = PRJ_NAME + '.yaml'

@pytest.fixture
def full_prj_repo(tmp_path):
    fname = 'lalala.yaml'
    validator = 'spec_validator.yaml'
    d = tmp_path / 'projects' 
    d.mkdir()
    f1 = d / fname
    f1.write_text('anything')
    f2 = d / validator
    f2.write_text(yaml.dump({'model':1, 'field_types':2}))
    return d

def test_create(full_prj_repo):
    d = full_prj_repo
    assert os.path.isfile(str(d/SPEC)) 
    assert not os.path.isfile(str(d/'fahrahrah.yaml'))
 
    creator._create_spec(d/'fahrahrah.yaml')

    assert os.path.isfile(str(d/SPEC)) 
    assert os.path.isfile(str(d/'fahrahrah.yaml'))

#######################################################################
#create_project and create
#######################################################################
pu = 'no/path/u'
pv = 'no/path/v'
pw = 'no/path/w'
px = 'no/path/x'
py = 'no/path/y'
pz = 'no/path/z'

def test_create_project_raises_spec_file_exists_exception(prjfix):
    err = io.StringIO()
    
    mk_spec()
    
    m1 = creator.msg1
    m2 = creator.msg2%SPEC_PTH
    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2) as xpct:
        
        creator.create_project(SPEC_PTH, px, py, pz)

    assert xpct.value.args[0] == m1+m2

def test_create_project_raises_sqlite_db_file_exists_exception(prjfix):
    err = io.StringIO()
    
    mk_sqlite_db()
    
    m1 = creator.msg3
    m2 = creator.msg4%IDX_PTH
    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2) as xpct:
        
        creator.create_project(pw, IDX_PTH, py, pz)

    assert xpct.value.args[0] == m1+m2

def test_create_project_raises_repo_db_exists_exception(prjfix):
    err = io.StringIO()
    
    mk_sqlite_db(path = cfg.TEXTS, fname = TEXTS_FNAME)

    m1 = creator.msg5
    m2 = creator.msg6%TEXTS_PTH

    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2)as xpct:
        
        creator.create_project(pw, px, TEXTS_PTH, pz)

    assert xpct.value.args[0] == m1+m2

def test_create_project_calls_create_project_successfully(monkeypatch, tmp_path):
    mock_create = mock.Mock(return_value = (True, True, True, True))
    monkeypatch.setattr(creator, '_create_project', mock_create)

    creator.create_project(pw, px, py, pz)

    msg = creator.msg14
    msg += creator.msg15%pw
    msg += creator.msg16%px
    msg += creator.msg17%py
    msg += creator.msg18%pz

    assert creator.create_project(pw, px, py, pz) == msg

def test_create_project_all_possible_unidentified_problem_msgs(tmp_path, monkeypatch):
    #1110
    mock_create = mock.Mock(return_value = (True, True, True, False))
    monkeypatch.setattr(creator, '_create_project', mock_create)
 
    msg = creator.msg9
    #msg += creator.msg10%pw
    #xmsg += creator.msg11%px
    #msg += creator.msg12%px
    msg += creator.msg13%pz

    assert creator.create_project(pw, px, py, pz) == msg

    #1101
    mock_create = mock.Mock(return_value = (True, True, False, True))
    monkeypatch.setattr(creator, '_create_project', mock_create)
 
    msg = creator.msg9
    #msg += creator.msg10%pw
    #xmsg += creator.msg11%px
    msg += creator.msg12%py
    #msg += creator.msg13%pz

    assert creator.create_project(pw, px, py, pz) == msg

    #1100
    mock_create = mock.Mock(return_value = (True, True, False, False))
    monkeypatch.setattr(creator, '_create_project', mock_create)
 
    msg = creator.msg9
    #msg += creator.msg10%pw
    #xmsg += creator.msg11%px
    msg += creator.msg12%py
    msg += creator.msg13%pz

    assert creator.create_project(pw, px, py, pz) == msg

    #1011
    mock_create = mock.Mock(return_value = (True, True, False, False))
    monkeypatch.setattr(creator, '_create_project', mock_create)
 
    msg = creator.msg9
    #msg += creator.msg10%pw
    #xmsg += creator.msg11%px
    msg += creator.msg12%py
    msg += creator.msg13%pz

    assert creator.create_project(pw, px, py, pz) == msg

    #1010
    #1001
    #1000
    #0111
    #0110
    #0101
    #0100
    #0011
    #0010
    #0001
    #0000


def test_start_project_spec(monkeypatch):
    #1111
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and x and y  and z

    #1110
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and x and y  and not z

    #1101
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and x and not y  and z

    #1100    
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and x and not y  and not z

    #1011
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and not x and y  and z

    #1010
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and not x and y  and not z

    #1001
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and not x and not y  and z

    #1000
    monkeypatch.setattr(creator, '_create_spec', lambda w: True)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert w and not x and not y  and not z

    #0111
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and x and y  and z

    #0110
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and  x and  y and not z

    #0101
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and x and not y  and z

    #0100
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: True)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and x and not y  and not z

    #0011
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and not x and y  and z

    #0010
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: True)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and not x and y  and not z

    #0001
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: True)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and not x and not y  and z

    #0000
    monkeypatch.setattr(creator, '_create_spec', lambda w: False)
    monkeypatch.setattr(creator, '_create_index_db', lambda x: False)
    monkeypatch.setattr(creator, '_create_cleaner_guide', lambda y: False)
    monkeypatch.setattr(creator, '_create_texts_db', lambda z: False)
    w, x, y, z = creator._create_project('w', 'x', 'y', 'z')
    assert not w and not x and not y  and not z
