import os

from wscr.cfg import SCRAPES 

from wscr.app.scraper import scrape_pages, Scraper

from wscr.tests.convenience_functions import DUMMY_DB_PTH, setup_scrape_tests
from wscr.tests.convenience_functions import IDX_PTH, TEXTS_PTH, CLNR_PTH

def test_select_texts():
    #index = sqlite3.connect(DUMMY_DB_PTH)
    assert os.path.isfile(DUMMY_DB_PTH)
    s = Scraper(DUMMY_DB_PTH)
    records = s.get_pages()

    assert type(records[0][2]) is bytes
    assert len(records[0]) == 5
    assert type(records[0]) is tuple
    assert len(records) == 9
    assert type(records) is list

def test_scrape_pages(prjfix):
    setup_scrape_tests()

    scrape_pages(IDX_PTH, CLNR_PTH, TEXTS_PTH)


