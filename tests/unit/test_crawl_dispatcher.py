import io

import os
import sqlite3

import mock
import pytest
from contextlib import redirect_stdout, redirect_stderr

from wscr.app import crawler
from wscr import cfg

from wscr.tests.convenience_functions import mock_index, mock_visitor
from wscr.tests.convenience_functions import mk_spec, SPEC_VAL_FNAME, IDX_FNAME 
from wscr.tests.convenience_functions import SPEC_FNAME

#no proj spec exception
nspf = 'cannot crawl; spec file \n%s\n does not exist'
#no sqlite index exception
nidx = 'cannot crawl; index file \n%s\n does not exist'

#######################################################################
#crawl_dispatcher function
#######################################################################
def test_crawl_no_index_file_exception(tmp_path):
    err = io.StringIO()

    spec = os.path.join(tmp_path, SPEC_FNAME)
    index = os.path.join(tmp_path, IDX_FNAME)

    msg = nidx%(index)
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        crawler.crawl_dispatcher(spec, index)
    assert xpct.value.args[0] == msg

def test_crawl_no_spec_file_exception(create_mock_project):
    err = io.StringIO()

    pth = os.path.join(cfg.PROJECTS, SPEC_FNAME) 
    os.remove(pth)
    msg = nspf%(pth)

    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        crawler.crawl_dispatcher(pth, os.path.join(cfg.PROJECTS, IDX_FNAME))

    assert xpct.value.args[0] == msg

def test_crawl_prj_not_crawlable_fail(monkeypatch, create_mock_project):
    mk_spec()
 
    mock_is_crawlable = mock.Mock(return_value = False)
    monkeypatch.setattr(crawler, 'is_crawlable',  mock_is_crawlable) 

    crawler.crawl_dispatcher( os.path.join(cfg.PROJECTS, SPEC_FNAME),
                              os.path.join(cfg.PROJECTS, IDX_FNAME))

    assert mock_is_crawlable.called

def test_crawl_link_crawl_strategy(monkeypatch, create_mock_project):
    mk_spec( ('strategy', 'link_crawl') )
 
    mock_is_crawlable = mock.Mock(return_value = True)
    monkeypatch.setattr(crawler, 'is_crawlable',  mock_is_crawlable) 

    pgc = mock.Mock()
    pgc.crawl_pages()
    monkeypatch.setattr(crawler, 'PageCrawler',  pgc) 

    idx = mock.Mock()
    monkeypatch.setattr(crawler.indexer, 'Indexer',  idx) 

    pgv = mock.Mock()
    monkeypatch.setattr(crawler.pagevisitor, 'PageVisitor',  pgv) 

    prs = mock.Mock()
    monkeypatch.setattr(crawler.html_parser, 'HTMLParser',  prs) 

    crawler.crawl_dispatcher( os.path.join(cfg.PROJECTS, SPEC_FNAME),
                              os.path.join(cfg.PROJECTS, IDX_FNAME))

    pgc.assert_called_once()
    pgc.crawl_pages.called

    idx.assert_called_once()
    idx.crawl_pages.called

    pgv.assert_called_once()
    pgv.crawl_pages.called


def test_crawl_archive_crawl_strategy(monkeypatch, create_mock_project):
    mk_spec( ('strategy', 'archive_crawl'),
                  ('search_crawl', {'terms': 'a b c'.split()}) ) 
    
    monkeypatch.setattr(crawler, 'is_crawlable',  mock.Mock(return_value = True))

    cl = mock.Mock()
    monkeypatch.setattr(crawler, 'crawl_listing', cl) 
    
    monkeypatch.setattr(crawler.indexer, 'Indexer',  mock. Mock()) 
    
    pgc = mock.Mock()
    pgc.crawl_pages()
    monkeypatch.setattr(crawler, 'PageCrawler',  pgc) 

    crawler.crawl_dispatcher( os.path.join(cfg.PROJECTS, SPEC_FNAME),
                              os.path.join(cfg.PROJECTS, IDX_FNAME))

    cl.assert_called_once()
    pgc.assert_called_once()
    pgc.crawl_pages.assert_called_once()

def test_search_crawl_strategy(monkeypatch, create_mock_project):
    mk_spec(('strategy', 'search_crawl'),
                ('rules', {'accept' : [], 'refuse' : []}) )
 
    isc = mock.Mock(return_value = True)
    monkeypatch.setattr(crawler, 'is_crawlable',  isc) 
    
    gsr = mock.Mock()
    monkeypatch.setattr(crawler, 'get_search_results',  gsr)

    idx = mock.Mock()
    monkeypatch.setattr(crawler.indexer, 'Indexer',  idx)

    pgc = mock.Mock()
    pgc.crawl_pages()
    monkeypatch.setattr(crawler, 'PageCrawler' ,  pgc)

    crawler.crawl_dispatcher(os.path.join(cfg.PROJECTS, SPEC_FNAME),
                             os.path.join(cfg.PROJECTS, IDX_FNAME))

    gsr.assert_called_once()
    idx.assert_called_once()
    pgc.assert_called_once()
    pgc.crawl_pages.assert_called_once()

def test_unknown_strategy_exception(monkeypatch, create_mock_project):
    err = io.StringIO()
    
    mk_spec( ('strategy', 'unknown_strategy') )
    
    mock_is_crawlable = mock.Mock(return_value = True)
    monkeypatch.setattr(crawler, 'is_crawlable',  mock_is_crawlable) 
    
    mock_get_search_results = mock.Mock()
    monkeypatch.setattr(crawler, 'get_search_results',  mock_get_search_results) 

    pgc = mock.Mock()
    monkeypatch.setattr(crawler, 'PageCrawler', pgc)

    msg = 'unknown crawl strategy: %s'%'unknown_strategy'
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        crawler.crawl_dispatcher(   os.path.join(cfg.PROJECTS, SPEC_FNAME), 
                                    os.path.join(cfg.PROJECTS, IDX_FNAME))

    assert xpct.value.args[0] == msg

    assert mock_is_crawlable.called
    assert not mock_get_search_results.called

