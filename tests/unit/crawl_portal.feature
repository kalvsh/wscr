Feature: Crawl web portal with PageCrawler.crawl_pages

#proper Prpject requirements
	#a crawl project spec
	#an index object indexes all pg visits(sqlite3 'interface')
	#a pagevisitor(HTTP client)
	#a list of zero or more seed_urls
	#a repository for the project exists(dir with sqlite3 db)


	
	Scenario: HTTP client does not return 200
		Given PageCrawler properly initiated
		Given that visitor returns response status DIFFERENT FROM 200
		When crawler.crawl_pages is called
        Then HTMLParser IS NOT called
        Then index.index_page IS NOT called
		Then crawl_pages method returns True

############
	Scenario: HTTP client returns 200
		Given PageCrawler properly initiated
		When crawler.crawl_pages is called
		Given visitor returns response status 200
    	Then HTMLParser IS called
		Then index.index_page IS called
    	Then crawl_pages method returns True

	Scenario: url indexed or bounced not visited again 	
		Given PageCrawler properly initiated
    	Given url is in crawler obj self.bounced OR self.indexed lists
		Given that there are NO MORE urls in self.urls
		Given the PageCrawler limit attr is false or len(indexed) < max
		When crawler.crawl_pages is called
    	Then visitor is not called
    	And HTMLParser is not called
    	And index.index_page is not called
    	And crawl_pages method returns True

	Scenario: urlA bounced or indexed, urlB crawled
		Given PageCrawler properly initiated
    	Given urls are in crawler obj self.bounced OR self.indexed lists
		Given that THERE ARE MORE urls in self.urls
		When crawler.crawl_pages is called
    	Then other urls are crawled & appended to self.indexed OR self.bounced
    	Then crawl_pages method returns True

	Scenario: crawls up to max limit of page visits
		Given PageCrawler properly initiated
		Given number of urls are not in self.bounced or self.indexed
		Given self.limit is TRUE and self.visit_count >= self.max
		When crawler.crawl_pages is called
    	Then it does NOT get crawled
    	Then visitor is NOT called
    	Then HTMLParser is NOT called
    	Then index.index_page is NOT called
    	Then crawl_pages method returns True

	Scenario: max limit of page visits not yet reached
		Given PageCrawler properly initiated
		Given url is not in self.bounced or self.indexed
		Given self.limit is TRUE and self.visit_count < self.max
		When crawler.crawl_pages is called
    	Then it does NOT get crawled
    	Then visitor is NOT called
    	Then HTMLParser is NOT called
    	Then index.index_page is NOT called
		Then crawl_pages method returns True
	
