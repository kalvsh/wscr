import io
import os
import mock
import pytest
import sqlite3

from contextlib import redirect_stderr

from wscr import cfg
from wscr.app import indexer, schema
from wscr.tests.convenience_functions import IDX_FNAME, IDX_PTH, mk_spec, mk_sqlite_db
from wscr.tests.convenience_functions import MOCK_SPEC, SPEC_FNAME, PRJ_NAME, URLS, MARKUPS

HTML =  MARKUPS[URLS[0]]
TITLE = PRJ_NAME
stg = 'link_crawl'

def test_generate_code(prjfix):
    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    start = (int(idx.start.strftime('%s')))
    code = idx.generate_code('lalala')
    prj, b36 = code.split(':')
    assert start == int(b36, base = 36)

def test_indexer_start_up(create_mock_project,  monkeypatch):
    dbf_path = os.path.join(cfg.PROJECTS, IDX_FNAME)
    
    db = mock.Mock()
    monkeypatch.setattr(indexer, 'dbmgr', db)
    #db.return_value.DBManager.return_value = mock.Mock()
    
    idxr = indexer.Indexer(dbf_path)

    #if do_insert is present, idxr has inherited it from dbmgr.DBMgr via DBAccess
    assert 'do_insert' in dir(idxr)

    #assert db.DBManager.called

def test_open_index(prjfix):
    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, 'link_crawl', False, None)

    with sqlite3.connect(os.path.join(cfg.PROJECTS, IDX_FNAME)) as conn: 
 
        r = conn.execute( schema.SELECT_INDEX['highest_crawlid'] )
        row = r.fetchone() 
    conn.close()

    assert row[0] == 1

    idx.open('cip', 'link_crawl', False, None)

    with sqlite3.connect(os.path.join(cfg.PROJECTS, IDX_FNAME)) as conn: 
 
        r = conn.execute( schema.SELECT_INDEX['highest_crawlid'] )
        row = r.fetchone() 
    conn.close()

    assert row[0] == 2

def test_open(prjfix): 
    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    
    #assert not idx.crawlid 
    
    idx.open(PRJ_NAME, 'link_crawl', 'Ophelia', None)

    assert idx.crawlid == 1

    idx.con.close()

    idx2 = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
 
    idx2.open(PRJ_NAME, 'link_crawl', 'McMiu', None)

    assert idx2.crawlid == 2

def test_index_page_basic(prjfix):
    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, 'link_crawl', False, None)

    pg = idx.index_page(URLS[0], MARKUPS[URLS[0]], TITLE)
    idx.close()

    assert pg == True

    check_url_insertion()

def test_index_register_bad_url_basic(prjfix):
    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, 'link_crawl', False, None)

    pg = idx.index_register_bad_url(URLS[0][1:], '404')
    idx.close()

    assert pg == 1

    with sqlite3.connect(os.path.join(cfg.PROJECTS, IDX_FNAME)) as conn: 
 
        r = conn.execute( 'SELECT * FROM BadURLs' )
        bad_row = r.fetchone() 

    conn.close()

    assert bad_row[0] == 1
    assert bad_row[1] == URLS[0][1:]
    assert bad_row[2] == '404'
    assert bad_row[3] == 1

def check_url_insertion():
    with sqlite3.connect(os.path.join(cfg.PROJECTS, IDX_FNAME)) as conn: 
 
        r = conn.execute( 'SELECT * FROM urls' )
        url_row = r.fetchone() 

        r = conn.execute( 'SELECT * FROM markups' )
        mup_row = r.fetchone() 

        r = conn.execute( 'SELECT * FROM titles' )
        ttl_row = r.fetchone() 

    conn.close()

    assert url_row[0] == 1
    assert url_row[1] == URLS[0]
    assert url_row[2] == 1

    assert mup_row[0] == 1
    assert mup_row[1] == MARKUPS[URLS[0]]
    assert mup_row[2] == 1

    assert ttl_row[0] == 1
    assert ttl_row[1] == TITLE
    assert ttl_row[2] == 1
    assert ttl_row[3] == 1

def test_index_page_integrity_error_caught(prjfix):
    err = io.StringIO()

    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, stg, False, None)

    pg = idx.index_page(URLS[0], MARKUPS[URLS[0]], TITLE)

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, stg, False, None)
    
    msg = 'UNIQUE constraint failed: URLs.url'

    #with redirect_stderr(err), pytest.raises(Exception, match = msg):
 
    assert not idx.index_page(URLS[0], MARKUPS[URLS[0]], TITLE)

def test_index_page_integrity_no_records_added(prjfix):
    err = io.StringIO()

    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, stg, False, None)

    pg = idx.index_page(URLS[0], MARKUPS[URLS[0]], TITLE)

    check_url_insertion()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, stg, False, None)

    check_url_insertion()

def test_index_url_vist_calls_index_page(create_mock_project, monkeypatch):
    db = mock.Mock()
    monkeypatch.setattr(indexer, 'dbmgr', db)
    db.return_value.DBManager.return_value = mock.Mock()

    idx = indexer.Indexer(IDX_PTH)
    idx.index_page = mock.Mock()
    idx.index_bad_url = mock.Mock()

    idx.index_url_visit('lalala', 'lalala', 200, True)

    assert idx.index_page.called
 
def test_index_url_vist_calls_index_bad_url_404(create_mock_project, monkeypatch):
    db = mock.Mock()
    monkeypatch.setattr(indexer, 'dbmgr', db)
    db.return_value.DBManager.return_value = mock.Mock()

    idx = indexer.Indexer(IDX_PTH)
    idx.index_page = mock.Mock()
    idx.index_register_bad_url = mock.Mock()

    idx.index_url_visit('lalala', 'lalala', 404, True)

    assert idx.index_register_bad_url.called
    assert not idx.index_page.called 

def test_index_url_visit_calls_index_bad_url_no_title(create_mock_project, monkeypatch):
    db = mock.Mock()
    monkeypatch.setattr(indexer, 'dbmgr', db)
    db.return_value.DBManager.return_value = mock.Mock()

    idx = indexer.Indexer(IDX_PTH)
    idx.index_page = mock.Mock()
    idx.index_register_bad_url = mock.Mock()

    idx.index_url_visit('lalala', 'lalala', 200, '')

    assert idx.index_register_bad_url.called
    assert not idx.index_page.called 
