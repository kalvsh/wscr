import os

import io
import mock
import yaml
import pytest
from contextlib import redirect_stdout, redirect_stderr

from wscr.app import main
import cfg

#bad mode exception
bm = 'bad mode parameter. Got %s, expected: {create, crawl}'


#######################################################################
#run_cli
#######################################################################
def mock_argparser(arglist):
    class MockParser: pass
    mp = MockParser()
    mp.mode = arglist[0]
    mp.project = arglist[1]
    return mp

def test_raises_bad_project_parameter_exception(monkeypatch):
    err = io.StringIO()
    #project name should not have file extension 
    args = ['crawl', 'lalala.yaml']
    monkeypatch.setattr(main, 'get_parser', lambda x: mock_argparser(args)) 
    with redirect_stderr(err), pytest.raises(Exception, match = main.MSG1) as xpct:
        main.run_cli()

def test_raises_bad_mode_parameter_exception(monkeypatch):
    err = io.StringIO()
    #project name should not have file extension 
    args = ['run', 'lalala']
    monkeypatch.setattr(main, 'get_parser', lambda x: mock_argparser(args)) 
    msg = main.MSG2%(args[0],main.ARGS )
    with redirect_stderr(err), pytest.raises(Exception, match = msg ) as x:
        main.run_cli()
    assert x.value.args[0] == main.MSG2%(args[0], main.ARGS)

def test_cli_crawl_start_crawl_is_called(monkeypatch):
    args = ['crawl', 'lalala']

    monkeypatch.setattr(main, 'get_parser', lambda x: mock_argparser(args)) 
    mock_crawl = mock.Mock(return_value = True)
    monkeypatch.setattr(main, 'crawl_dispatcher', mock_crawl)
    assert not mock_crawl.called

    d = 'not/a/dir'
    main.run_cli(d)

    spec = os.path.join(d, args[1] + '.yaml')
    index = os.path.join(d, args[1] + '.sqlite')
 
    assert mock_crawl.called
    mock_crawl.assert_called_with(spec, index)

def test_cli_start_create_called(monkeypatch, tmp_path):
    args = ['create', 'lalala']
    monkeypatch.setattr(main, 'get_parser', lambda x: mock_argparser(args)) 

    mock_create = mock.Mock(return_value = True)
    monkeypatch.setattr(main, 'create_project', mock_create)
 
    main.run_cli()
 
    assert mock_create.called

def test_cli_start_scrape_called(monkeypatch, tmp_path):
    args = ['scrape', 'lalala']
    monkeypatch.setattr(main, 'get_parser', lambda x: mock_argparser(args)) 

    mock_scrape = mock.Mock(return_value = True)
    monkeypatch.setattr(main, 'scrape_pages', mock_scrape)
 
    main.run_cli()
 
    assert mock_scrape.called


def test_log_is_created():
    assert False
