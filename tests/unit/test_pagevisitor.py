import io
import pytest

from http_server_mock import HttpServerMock
from contextlib import redirect_stdout, redirect_stderr

from  wscr.app import pagevisitor

app = HttpServerMock(__name__)

@app.route('/', methods=['GET'])
def index():
    return 'hello world'

#@pytest.mark.skip
def test_page_visitor_200():
    with app.run('localhost', 5000):
        pv = pagevisitor.PageVisitor()

        r = pv.visit('http://localhost:5000/')

        assert r.status == 200
        assert r.data == b'hello world'

#@pytest.mark.skip
def test_connection_refused_msg():
    with app.run('localhost', 5000):
        pv = pagevisitor.PageVisitor()
        r = pv.visit('http://localhost:2020/')
        assert r.status == 'urllib3 - [Errno 111]' 
        assert r.data == 'Connection refused'

        r = pv.visit('http://lalala:2020/')
        assert r.status == 'urllib3 - [Errno 111]' 
        assert r.data == 'Connection refused'

#@pytest.mark.skip
def test_404():
    with app.run('localhost', 5000):
        pv = pagevisitor.PageVisitor()
        r = pv.visit('http://localhost:5000/abc')

        assert r.status == 404 
        assert type(r.data) == type(b'')

#@pytest.mark.skip
def test_no_server():
    pv = pagevisitor.PageVisitor()
    r = pv.visit('http://localhost:2020/')

    assert r.status == 'urllib3 - [Errno 111]' 
    assert r.data == 'Connection refused'
