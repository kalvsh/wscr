import pytest

import os
import sqlite3

from wscr.app import dbmgr
from wscr.app import schema

dbf = 'index.sqlite'

@pytest.fixture
def dummydb(tmp_path):
    p = os.path.join(tmp_path, dbf)
    db = sqlite3.connect(p)
    return db

@pytest.fixture
def dummydb(tmp_path):
    p = os.path.join(tmp_path, dbf)
    db = sqlite3.connect(p)

    return db

######################################################################
#
######################################################################
def test_creates_db(tmp_path):
    p = os.path.join(tmp_path, dbf)
    assert not os.path.exists(p)
    dbmgr.create_db(p)
    assert os.path.exists(p)

def test_creates_db_with_schema(tmp_path):
    p = os.path.join(tmp_path, dbf)
    assert not os.path.exists(p)
    dbmgr.create_db(p)
    assert os.path.exists(p)
    conn = sqlite3.connect(p)
    cursor = conn.cursor()
    sql = 'SELECT * FROM sqlite_master WHERE type="table" and name=?'
    for tbl in schema.CREATE_INDEX.keys():
        cursor.execute(sql, [tbl])
        r = cursor.fetchall()
        assert len(r) == 1
        assert r[0][1] == tbl
        assert (r[0][4]) == (schema.CREATE_INDEX[tbl])

def test_raises_wrong_extension_exception(tmp_path, dummydb):
    p1 = os.path.join(tmp_path, dbf)
    p2 = os.path.join(tmp_path, 'index.db')
    os.rename(p1, p2)
    with pytest.raises(Exception, match = dbmgr.msg1):
        dbmgr.create_db(p2)

def test_raises_db_exists_exceptions(tmp_path, dummydb):
    p = os.path.join(tmp_path, dbf)
    with pytest.raises(Exception, match = dbmgr.msg2 ):
        dbmgr.create_db(p)

######################################################################
#
######################################################################
def test_raises_path_arg_wrong_extension_exception(tmp_path):
    p = os.path.join(tmp_path, 'index.db')
    with pytest.raises(Exception, match = dbmgr.msg3):
        dbm = dbmgr.DBManager(p)

def test_raises_no_dbindex_exists_exception(tmp_path):
    p = os.path.join(tmp_path, dbf)
    with pytest.raises(Exception, match = dbmgr.msg4a+dbmgr.msg4b):
        dbm = dbmgr.DBManager(p)

def test_connects_to_dbindex(tmp_path, dummydb):
    p = os.path.join(tmp_path, dbf)
    assert os.path.exists(p)
    dbm = dbmgr.DBManager(p)
    conn = sqlite3.connect(p)
    cursor = conn.cursor()
    #no schema!
    sql = 'SELECT * FROM sqlite_master WHERE type="table" and name=?'
    for tbl in schema.CREATE_INDEX.keys():
        cursor.execute(sql, [tbl])
        r = cursor.fetchall()
        assert len(r) == 0
