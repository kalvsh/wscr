import mock
import pytest
import unittest

from collections import deque

from wscr.tests.convenience_functions import SPEC_FNAME, MARKUPS, MOCK_SPEC
from wscr.tests.convenience_functions import URLS, STATUS, SEED, TITLES 
from wscr.tests.convenience_functions import MockParser, mock_index, mock_visitor
from wscr.tests.convenience_functions import mock_response_200 
from wscr.tests.convenience_functions import mock_response_404

from wscr.app import crawler
from wscr.app import schema

class TestNoSeedURL(unittest.TestCase):
    def setUp(self):
        self.idx = mock_index()
        self.vstr = mock_visitor()
        self.parser = MockParser(MOCK_SPEC['rules'])

        #instantiated with seed_urls == []
        self.pc = crawler.PageCrawler(MOCK_SPEC, self.idx, self.vstr, self.parser, [])

    def no_calls_composite_objs_methods(self):
        assert not self.pc.pgvisitor.visit.called
        assert not self.pc.index.index_url_visit.called
        assert not self.pc.parser.make_soup_called
        assert not self.pc.parser.find_url_links_called
        assert not self.pc.parser.get_page_title_called
        return True

    def test_no_bounced_no_indexed_no_methods_called(self):
        assert self.pc.indexed == self.pc.bounced == []
        crawl = self.pc.crawl_pages()

        assert self.no_calls_composite_objs_methods()
        assert crawl == True

    def test_no_bounced_one_indexed_no_methods_called(self):
        self.pc.indexed.append(URLS[0])  
        self.pc.bounced == []

        crawl = self.pc.crawl_pages()

        assert self.no_calls_composite_objs_methods()
        assert crawl == True

    def test_one_bounced_no_indexed_no_methods_called(self):
        self.pc.indexed.append(URLS[0])  
        self.pc.bounced == []

        crawl = self.pc.crawl_pages()

        assert self.no_calls_composite_objs_methods()
        assert crawl == True

    def test_one_bounced_one_indexed_no_methods_called(self):
        self.pc.indexed.append(URLS[0])  
        self.pc.bounced == [URLS[1]]

        crawl = self.pc.crawl_pages()

        assert self.no_calls_composite_objs_methods()
        assert crawl == True

class TestOneSeedURL(unittest.TestCase):
    def setUp(self):
        self.idx = mock_index()
        self.vstr = mock_visitor()
        self.prs = MockParser(MOCK_SPEC['rules'])

        #instantiated with seed_url [SEED]
        self.pc = crawler.PageCrawler(MOCK_SPEC, self.idx, self.vstr,self.prs, [SEED])

    def calls_composite_objs_methods(self):
        assert self.pc.pgvisitor.visit.called
        assert self.pc.index.index_url_visit.called
        assert self.pc.parser.make_soup_called
        assert self.pc.parser.find_url_links_called
        assert self.pc.parser.get_page_title_called
        return True

    def test_no_bounced_no_indexed_methods_called(self):
        assert self.pc.indexed == self.pc.bounced == []
        crawl = self.pc.crawl_pages()

        assert self.calls_composite_objs_methods()
        assert crawl == True

    def test_no_bounced_one_indexed_methods_called(self):
        self.pc.indexed.append(URLS[1])  
        self.pc.bounced == []

        crawl = self.pc.crawl_pages()

        assert self.calls_composite_objs_methods()
        assert crawl == True

    def test_one_bounced_no_indexed_methods_called(self):
        self.pc.bounced.append(URLS[1])  
        self.pc.indexed == []

        crawl = self.pc.crawl_pages()

        assert self.calls_composite_objs_methods()
        assert crawl == True

    def test_one_bounced_one_indexed_methods_called(self):
        self.pc.indexed.append(URLS[1])  
        self.pc.bounced == [URLS[2]]

        crawl = self.pc.crawl_pages()

        assert self.calls_composite_objs_methods()
        assert crawl == True

class TestNoCrawlLimit(unittest.TestCase):
    def setUp(self):
        self.idx = mock_index()
        self.vstr = mock_visitor()
        self.prs = MockParser(MOCK_SPEC['rules'])

        self.pc = crawler.PageCrawler(MOCK_SPEC, self.idx, self.vstr, self.prs, [SEED])

        #Given PageCrawler attr limit has value FALSE 
        assert 'limit' in dir(self.pc)
        assert self.pc.limit == False

    def calls_composite_objs_methods(self):
        assert self.pc.pgvisitor.visit.called
        assert self.pc.index.index_url_visit.called
        assert self.pc.parser.make_soup_called
        assert self.pc.parser.find_url_links_called
        assert self.pc.parser.get_page_title_called
        return True

    def no_calls_composite_objs_methods(self):
        assert not self.pc.pgvisitor.visit.called
        assert not self.pc.index.index_url_visit.called
        assert not self.pc.parser.make_soup_called
        assert not self.pc.parser.find_url_links_called
        assert not self.pc.parser.get_page_title_called
        return True

    def test_http_client_is_called_for_seed_url(self):
        #When crawler is called
        crawl = self.pc.crawl_pages()  
        
        #Then the http client pagevisitor IS called
        args, kwargs = self.pc.pgvisitor.visit.call_args
        assert SEED in args and len(args) == 1 and len(kwargs) == 0
    
    def include_bounced_indexed(self, i1, i2):
        url_a, url_b = URLS[1:3] 

        self.pc.urls.append(url_a)
        self.pc.urls.append(url_b)

        self.pc.bounced.append(url_b)
        self.pc.indexed.append(url_a)

    def test_composites_not_called_for_bounced_and_indexed_urls(self):
        #Given url is in crawler obj self.bounced OR self.indexed lists
	#And that there are NO MORE urls in self.urls
        self.pc.urls = deque([])

        self.include_bounced_indexed(1, 3)

        #When crawler is called
        crawl = self.pc.crawl_pages()  
        
        assert self.no_calls_composite_objs_methods()

    def test_no_pg_visit_limit_visits_one(self):
        #When crawler is called
        crawl = self.pc.crawl_pages()  
        
        #And  url in seed_urls param is crawled
        assert len(self.pc.bounced) == 0
        assert self.pc.visit_count == 1

        #And composite object methods are called
        self.calls_composite_objs_methods()

        #And  crawler makes call to indexer.index_url_visit with expected args
        args, kwargs = self.pc.index.index_url_visit.call_args
        for _ in [SEED, MARKUPS[SEED], STATUS[SEED],  TITLES[SEED]]: 
            assert _ in args 
        assert len(args) == 4 and len(kwargs) == 0

        assert len(self.pc.indexed) == 1 and SEED in self.pc.indexed

        #And crawl_pages returns True
        assert crawl == True

    def test_only_non_bounced_or_indexed_urls_crawled(self):
        #Given urls are in crawler obj self.bounced OR self.indexed lists
	#Given that THERE ARE MORE urls in self.urls (seed url)
        #When crawler is called
        self.include_bounced_indexed(1, 3)

        crawl = self.pc.crawl_pages()  
 
        #Then the http client pagevisitor is called
        #And the non bounced, non indexed seed url is crawled
        args, kwargs = self.pc.pgvisitor.visit.call_args
        assert SEED in args and len(args) == 1 and len(kwargs) == 0
 
        #And only it is crawled
        assert self.pc.visit_count == 1
 
        args = self.pc.parser.instanced_with
        assert self.pc.parser.rules ==  args[0]
        assert len(args) == 1

        #And  crawler makecall to indexer.index_url_visit
        assert len(self.pc.indexed) == 2 
        assert SEED in self.pc.indexed

        assert self.pc.index.index_url_visit.called

        #And  crawler makes call to indexer.index_url_visit
        args, kwargs = self.pc.index.index_url_visit.call_args
        for _ in [SEED, MARKUPS[SEED], STATUS[SEED],  TITLES[SEED]]: 
            assert _ in args 
        assert len(args) == 4 and len(kwargs) == 0

        #And crawl_pages returns True
        assert crawl == True

    def test_crawls_up_to_limit_of_visits(self):
        #Given the PageCrawler limit attr is false or len(indexed) < max
        self.pc.limit = True
        self.pc.max = 26
         
	#Given a number urls are not in self.bounced or self.indexed
        self.pc.urls = deque([])
        for _ in URLS[0:5]: self.pc.indexed.append( _ ) #a to e
        for _ in URLS[5:10]: self.pc.bounced.append( _ ) # f to j
        for _ in URLS[10:]: self.pc.urls.append( _ ) #k to z
 
        #When crawler is called
        crawl = self.pc.crawl_pages()  

        #Then the http client pagevisitor is called
        #And the non bounced, non indexed urls are crawled
        args, kwargs = self.pc.pgvisitor.visit.call_args
        assert args[0] == URLS[-1]#arg is that of last call
        #And only they are crawled
        assert self.pc.visit_count == len(URLS[10:])
 
        #And  crawler makecall to indexer.index_url_visit
        args, kwargs = self.pc.index.index_url_visit.call_args
        assert len(args) == 4 and len(kwargs) == 0
        assert args[0] in URLS[10:]
        assert args[1] == MARKUPS[args[0]]
        assert args[2] == STATUS[args[0]]
        assert args[3] == TITLES[args[0]]

        #And crawl_pages returns True
        assert crawl == True

def testHTTPNotReturn200(monkeypatch):
    idx = mock_index()
    vstr = mock_visitor(side_effect = mock_response_404)
    prs = MockParser(MOCK_SPEC['rules'])

    pc = crawler.PageCrawler(MOCK_SPEC, idx, vstr, prs, [SEED])
    pc.urls = deque(URLS)
 
    crawl = pc.crawl_pages()
 
    assert pc.visit_count == len(STATUS)
    assert len(pc.indexed) == 0
    assert len(pc.bounced) == len(STATUS)

if __name__ == '__main__':
    unittest.main()

