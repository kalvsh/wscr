import os
import io
import yaml
import pytest

from contextlib import redirect_stdout, redirect_stderr

from wscr.app import validator

from wscr.tests.convenience_functions import SPEC_VAL_FNAME, MOCK_SPEC, SPEC_VAL

SPEC = {k:v for k, v in MOCK_SPEC.items()}
 
#######################################################################
#
#######################################################################
def test_is_matched_():
    m = validator.is_matched_list('a b c'.split(), 'a b c'.split())
    assert m[0] == True and  m[1] == ''

    m = validator.is_matched_list('a b'.split(), 'a b c'.split())
    assert m[0] == False and m[1] == validator.MSG3%(validator.MSG1, 'c')
    
    m = validator.is_matched_list('a b c'.split(), 'a b d'.split())
    assert m[0] == False and m[1] == validator.MSG2%(validator.MSG1, 'c')
    
#######################################################################
#is_valid
#######################################################################

def test_is_valid_raises_error_from_is_matched_list(monkeypatch):
    err = io.StringIO()

    msg = 'no match'

    monkeypatch.setattr(validator, 'is_matched_list', lambda x, y: (False, msg) )


    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
 
        validator.is_valid(SPEC, SPEC_VAL['model'], SPEC_VAL['field_types'] )
    
def test_is_valid_spec_with_expected_keys(monkeypatch):
    monkeypatch.setattr(validator, 'is_matched_list', lambda x, y: (True, ''))
    assert validator.is_valid(SPEC, SPEC_VAL['model'],
                                    SPEC_VAL['field_types'] )

def test_is_valid_spec_with_unexpected_value_raise_exception(monkeypatch):
    monkeypatch.setattr(validator, 'is_matched_list', lambda x, y: (True, ''))
    SPEC['project'] = 1

    msg = "field project has value 1. Type <class 'int'>, expected <class 'str'>"

    with pytest.raises(Exception) as xcpt:
        validator.is_valid(SPEC, SPEC_VAL['model'], SPEC_VAL['field_types'] )
    assert xcpt.value.args[0] == msg

    SPEC['project'] = ''

def test_is_valid_dictionaries_all_the_way_down(monkeypatch):
    monkeypatch.setattr(validator, 'is_matched_list', lambda x, y: (True, ''))
    SPEC_VAL['model']['turtle1'] = {'turtle2': {'turtle3': 1 }, 't2': []}
    
    tups = [('turtle1', {}), ('turtle2', {}) ,('turtle3', 1), ('t2', [])]
    for t in tups:
        SPEC_VAL['field_types'][t[0]]  = t[1]    
    SPEC['turtle1'] = {'turtle2': {'turtle3': 1 }, 't2' : []}
 
    msg = "field project has value 1. Type <class 'int'>, expected <class 'str'>"
    
    assert validator.is_valid(SPEC, SPEC_VAL['model'], SPEC_VAL['field_types'] )

    SPEC['project'] = ''
    SPEC_VAL['model'].popitem() 
    SPEC_VAL['field_types'].popitem()
    SPEC_VAL['field_types'].popitem()
    SPEC_VAL['field_types'].popitem()
    SPEC_VAL['field_types'].popitem()
    SPEC.popitem()

#######################################################################
#is_crawlable
#######################################################################
def test_is_crawlable_status_not_ready_for_crawl_exception(monkeypatch):
    monkeypatch.setattr(validator, 'is_valid', lambda x, y, z: True)
    msg = 'spec has status 0 (not ready for crawl); please review it.'
    with pytest.raises(Exception) as xcpt:

        validator.is_crawlable(SPEC, SPEC_VAL['model'], SPEC_VAL['field_types'])

    assert xcpt.value.args[0] == msg

def test_is_crawlable_not_valid_spec_exception(monkeypatch):
    monkeypatch.setattr(validator, 'is_valid', lambda x, y, z: False)
    msg = 'project spec is not valid; please review it.'
    with pytest.raises(Exception) as xcpt:

        validator.is_crawlable(SPEC, SPEC_VAL['model'], SPEC_VAL['field_types'])

    assert xcpt.value.args[0] == msg

def test_is_crawlable_status_not_ready_for_crawl_exception(monkeypatch):
    monkeypatch.setattr(validator, 'is_valid', lambda x, y, z: True)
    SPEC['status'] = 1

    assert validator.is_crawlable(  SPEC, 
                                    SPEC_VAL['model'], 
                                    SPEC_VAL['field_types'] )

