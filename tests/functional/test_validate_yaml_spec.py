import os
import pytest

from wscr.app import validator

from wscr.app import validator

from wscr.tests.convenience_functions import SPEC_FNAME, MOCK_SPEC

def test_reads_boolean_field_type(prjfix):
    spec = MOCK_SPEC

    assert spec['status'] == False
    assert type(spec['status']) ==type(False)

    assert spec['limit'] == 0
    assert type(spec['limit']) == type(False)
