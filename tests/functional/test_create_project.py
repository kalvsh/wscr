import os
import io
import pytest
import sqlite3
import argparse

from contextlib import redirect_stdout, redirect_stderr

from wscr import cfg
from wscr.app import fio
from wscr.app import schema
from wscr.app import validator

from wscr.app import main
from wscr.app import creator

from wscr.tests.convenience_functions import PRJ_NAME, SPEC_PTH, IDX_FNAME, SPEC_FNAME, ARGS1
from wscr.tests.convenience_functions import IDX_PTH, TEXTS_FNAME, mk_spec, mk_sqlite_db
from wscr.tests.convenience_functions import TEXTS_PTH, CLNR_GUIDE_FNAME, CLNR_PTH, get_clnr_val

pw = 'no/path/w'
px = 'no/path/x'
py = 'no/path/y'
pz = 'no/path/z'


def test_create_new_project(capsys, prjfix):
    args = main.get_parser(ARGS1)
 
    main.run_cli(args = args)
    
    projects = os.listdir(cfg.PROJECTS)
    assert SPEC_FNAME in projects
    assert IDX_FNAME in projects
    
    assert TEXTS_FNAME in os.listdir(cfg.TEXTS)

def test_new_project_spec_has_expected_fields(prjfix):
    args = main.get_parser(ARGS1)
 
    main.run_cli(args = args)

    f = os.path.join(cfg.PROJECTS, SPEC_FNAME)
    
    assert os.path.isfile(f)
    
    spec = fio.read_yaml(f)

    valid = fio.read_yaml(os.path.join(cfg.PROJECTS, 'spec_validator.yaml'))

    assert validator.validate(spec, valid['model'], valid['field_types'])

def test_new_project_index_has_expected_schema(prjfix):
    args = main.get_parser(ARGS1)
 
    main.run_cli(args = args)

    db = os.path.join(cfg.PROJECTS, IDX_FNAME)

    assert os.path.isfile(db)
 
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
 
    sql = 'SELECT * FROM sqlite_master WHERE type="table" and name=?'

    for tbl in schema.CREATE_INDEX.keys():
        cursor.execute(sql, [tbl])
        r = cursor.fetchall()
        assert len(r) == 1
        assert r[0][1] == tbl
        assert (r[0][4]) == (schema.CREATE_INDEX[tbl])

def test_new_project_textdb_has_expected_schema(prjfix):
    args = main.get_parser(ARGS1)
 
    main.run_cli(args = args)

    db = os.path.join(cfg.TEXTS, TEXTS_FNAME)

    assert os.path.isfile(db)
 
    conn = sqlite3.connect(db)
    cursor = conn.cursor()
 
    sql = 'SELECT * FROM sqlite_master WHERE type="table" and name=?'

    for tbl in schema.CREATE_TEXT_DB.keys():
        cursor.execute(sql, [tbl])
        r = cursor.fetchall()
        assert len(r) == 1
        assert r[0][1] == tbl
        assert (r[0][4]) == (schema.CREATE_TEXT_DB[tbl])

def test_proj_name_is_file_name_fails(capsys, prjfix):
    err = io.StringIO()
    s = 'create ' + SPEC_FNAME
    args = main.get_parser(s.split())

    msg = 'project name should not end in or contain substring .yaml'
 
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        main.run_cli(args = args)
    
    projects = os.listdir(cfg.PROJECTS)
    assert SPEC_FNAME not in projects
    assert IDX_FNAME not in projects

def test_spec_already_exists_failure(prjfix):
    err = io.StringIO()
    s = 'create ' + PRJ_NAME
    args = main.get_parser(s.split())

    mk_spec()

    m1 = creator.msg1
    m2 = creator.msg2%SPEC_PTH
    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2) as xpct:
        
        main.run_cli(args = args)
        #creator.create_project(SPEC_PTH, px, py, pz)

    assert xpct.value.args[0] == m1+m2

    #with redirect_stderr(err), pytest.raises(Exception, match = msg1+msg2) as xpct:
    #    main.run_cli(args = args)

def test_index_already_exists_failure(prjfix):
    err = io.StringIO()
    s = 'create ' + PRJ_NAME
    args = main.get_parser(s.split())

    mk_sqlite_db()

    m1 = creator.msg3
    m2 = creator.msg4%IDX_PTH
    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2) as xpct:
      
        main.run_cli(args = args)
        #creator.create_project(pw, IDX_PTH, py, pz)

    assert xpct.value.args[0] == m1+m2

def test_clnr_guide_already_exists_failure(prjfix):
    err = io.StringIO()
    s = 'create ' + PRJ_NAME
    args = main.get_parser(s.split())

    mk_spec(path = CLNR_PTH, f = get_clnr_val)

    m1 = creator.msg5
    m2 = creator.msg6%CLNR_PTH

    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2)as xpct:
 
        main.run_cli(args = args)
        #creator.create_project(pw, px, tr, pz)

    assert xpct.value.args[0] == m1+m2

def test_repo_already_exists_failure(prjfix):
    err = io.StringIO()
    s = 'create ' + PRJ_NAME
    args = main.get_parser(s.split())

    mk_sqlite_db(path = cfg.TEXTS, fname = PRJ_NAME + '_texts.sqlite')

    m1 = creator.msg7
    m2 = creator.msg8%TEXTS_PTH

    with redirect_stderr(err), pytest.raises(Exception, match = m1+m2)as xpct:
 
        main.run_cli(args = args)
        #creator.create_project(pw, px, tr, pz)

    assert xpct.value.args[0] == m1+m2
