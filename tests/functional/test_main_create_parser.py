import io
import yaml
import pytest
from contextlib import redirect_stdout, redirect_stderr

from wscr.app import main

#usage error message
uerr1 = 'usage: pytest [-h] {create,crawl,scrape} project'
uerr2 = 'pytest: error: the following arguments are required: mode, project'

#help message
h1 = uerr1
h2 = ' positional arguments:'
h3 = ' {create,crawl,scrape} '+ main.HELP1
h4 = ' project type a project spec file name'
h5 = ' optional arguments:'
h6 = ' -h, --help show this help message and exit'

#missing project argument
pa1 = ' pytest: error: the following arguments are required: project'

#missing mode argument
ma1 = ' pytest: error: the following arguments are required: project'

#bad project argument
ba1 = ' pytest: error: argument mode: invalid choice:'
a0, a1, a2 = main.ARGS.split()
ba2 = " 'lelele' (choose from '%s', '%s', '%s')"%(a0, a1, a2)

#######################################################################
#
#######################################################################
def test_get_parser_no_argument_TypeError():
    err = io.StringIO()
    with redirect_stderr(err), pytest.raises(TypeError) as xpct:
        main.get_parser()
    assert err.getvalue() == ''

def test_get_parser_help_argument():
    out = io.StringIO()
    with redirect_stdout(out), pytest.raises(SystemExit) as xpct:
        main.get_parser(['-h'])
    assert " ".join( out.getvalue().split() ) == '%s%s%s%s%s%s'%(h1,h2,h3,h4,h5,h6)

def test_get_parser_only_mode():
    out = io.StringIO()
    with redirect_stderr(out), pytest.raises(SystemExit) as xpct:
        main.get_parser(['create'])
    assert " ".join( out.getvalue().split() )  == '%s%s'%(uerr1, pa1)

def test_get_parser_invalid_mode():
    out = io.StringIO()
    with redirect_stderr(out), pytest.raises(SystemExit) as xpct:
        main.get_parser(['lelele', 'lalala.yaml'])
    assert " ".join( out.getvalue().split() ) == '%s%s%s'%(uerr1, ba1,ba2)

def test_get_parser_create_mode():
    out1 = io.StringIO()
    out2 = io.StringIO()
    with redirect_stderr(out1), redirect_stdout(out2) as xpct:
        main.get_parser(['create', 'lalala.yaml'])
    assert " ".join( out2.getvalue().split() )  == out2.getvalue() == '' 
