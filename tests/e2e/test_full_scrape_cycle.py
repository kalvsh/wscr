import os

from wscr.tests.convenience_functions import TMP_WEB_DIR, SPEC_FNAME, IDX_FNAME, SPEC_VAL_FNAME
from wscr.tests.convenience_functions import MockVisitor, mk_spec, mk_sqlite_db, create_web_resources

from wscr import cfg
from wscr.app import crawler

#@pytest.mark.skip
def test_crawl_(webfix, monkeypatch, prjfix):
    os.mkdir(TMP_WEB_DIR)

    spec = mk_spec( ('domain', 'www.urla.com'), 
                        ('link_crawl',  {'seeds': ['www.urla.com/home.html']}), 
                        ('rules' , {'accept': {'prefix':['www.urla.com'],
                                                'regex': [] },
                                    'reject': {'prefix': [],
                                               'infix': [],
                                               'suffix': [],
                                               'regex': []}}) )
    mk_sqlite_db()

    create_web_resources()

    monkeypatch.setattr(crawler.pagevisitor, 'PageVisitor', MockVisitor)

    #crawl = crawler.PageCrawler(spec, index, mv, parser, ['http://'+URLS[0]+'/home.html' ])

    crawler.crawl_dispatcher( os.path.join(cfg.PROJECTS, SPEC_FNAME), 
                              os.path.join(cfg.PROJECTS, IDX_FNAME),)
