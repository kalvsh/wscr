import os
import pytest
import sqlite3

from wscr import cfg
from wscr.app import indexer

from wscr.tests.convenience_functions import IDX_FNAME, mk_sqlite_db
from wscr.tests.convenience_functions import PRJ_NAME, URLS, MARKUPS

HTML =  MARKUPS[URLS[0]]
TITLE = PRJ_NAME


def test_do_select_all_basic(prjfix): 
    mk_sqlite_db()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    idx.open(PRJ_NAME, 'link_crawl', False, None)

    for i, url in enumerate(URLS[:19]):
        pg = idx.index_page(url, MARKUPS[url], TITLE + str(i))
    idx.close()

    idx = indexer.Indexer(os.path.join(cfg.PROJECTS, IDX_FNAME))
    rows = idx.do_select_all('SELECT * FROM URLs')
    idx.con.close()

    assert len(rows) == 19
