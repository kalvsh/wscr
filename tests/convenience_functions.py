import os
import mock
import yaml
import copy
import shutil
import sqlite3

from webtest import TestApp
from collections import deque

from wscr.app import schema
from wscr import cfg

DOMAIN = 'www.lalala.com.br'
#DOMAIN = "http://httpbin.org"

TITLE_TEMPLATE = '<title> title for %s</title>'
LINK_TEMPLATE = '<a href="https://%s" >'

URLS = ['www.url%s.com'%c for c in list(map(chr,range(97,123)))]
TITLES = {u: TITLE_TEMPLATE%u  for u in URLS}
LINKS = {u: LINK_TEMPLATE%u for u in URLS}
STATUS = {u: 200 for u in URLS}

TMP_WEB_DIR = '/tmp/website'

HTML_TEMPLATE = ('<DOC TYPE html>'
                 '<html>'
                 '<title> title for %s</title>'
                 '<body>'
                 '<h1>This is a heading for %s</h1>'
                 '<p>This is a paragraph in %s.</p>'
                 '<p>There is a link to itself %s</p>'
                 '</body>'
                 '</html>'  )

MARKUPS = {u: HTML_TEMPLATE%(u,u,u, LINKS[u]) for u in URLS}

#dummies
SEED = URLS[0]

PRJ_NAME = 'lalala'
IDX_FNAME = PRJ_NAME + '.sqlite'
SPEC_FNAME = PRJ_NAME + '.yaml'
TEXTS_FNAME = PRJ_NAME + '_texts.sqlite'
CLNR_GUIDE_FNAME = PRJ_NAME + '_clean.yaml'

SPEC_VAL_FNAME = cfg.SPEC_VAL_FNAME
CLEANER_VAL_FNAME  = cfg.CLEANER_VAL_FNAME

SPEC_PTH = os.path.join(cfg.PROJECTS, SPEC_FNAME)
IDX_PTH = os.path.join(cfg.PROJECTS, IDX_FNAME)
CLNR_PTH = os.path.join(cfg.TEXTS, CLNR_GUIDE_FNAME)
TEXTS_PTH = os.path.join(cfg.TEXTS, TEXTS_FNAME)

DUMMY_DB_PTH = os.path.join(cfg.REPO, 'mocks', 'snopes.sqlite')

with open(os.path.join(cfg.PROJECTS, SPEC_VAL_FNAME)) as f:
    SPEC_VAL  = yaml.safe_load(f)
MOCK_SPEC = copy.deepcopy(SPEC_VAL['model'])

with open(os.path.join(cfg.TEXTS, CLEANER_VAL_FNAME)) as f:
    CLEANER_VAL  = yaml.safe_load(f)

#for creating a crawl project
CMD1 = 'create '+ PRJ_NAME
ARGS1 = CMD1.split()

#for crawling a crawl project
CMD2 = 'crawl ' + PRJ_NAME
ARGS2 = CMD2.split()

TEXTS_SELECT_CMDS = schema.SELECT_TEXT_DB
TBL_CREATE_IDX_CMDS = schema.CREATE_INDEX

def get_spec_val():
    spec = SPEC_VAL['model']
    spec['project'] = PRJ_NAME
    spec['domain'] = DOMAIN
    spec['strategy'] = 'link_crawl'
    spec['link_crawl'] = {'seeds': [DOMAIN]}
    spec['search_crawl'] = {'terms': []}
    spec['archive_crawl'] = {'listing': []}
    spec['limit'] = False
    spec['max'] = 0
    spec['status'] = True
    spec['rules'] = {}
    spec['rules']['accept'] = {}
    spec['rules']['accept']['prefix']= []
    #spec['rules']['accept']['infix'] = []
    #spec['rules']['accept']['suffix']=  []
    spec['rules']['accept']['regex'] = []

    spec['rules']['reject'] = {}
    spec['rules']['reject']['prefix']= []
    spec['rules']['reject']['infix'] = []
    spec['rules']['reject']['suffix']=  []
    spec['rules']['reject']['regex'] = []

    return spec

def get_clnr_val():
    guide = CLEANER_VAL['model']
    guide['parse_guide'] =  {}
    guide['parse_guide']['start_tag'] = ''
    guide['parse_guide']['end_tag'] = ''

    return guide

def mk_spec(*args, path = SPEC_PTH, f = get_spec_val):
    spec = f()
    for arg in args:
        spec[arg[0]] = arg[1]

    with open(path, 'w') as f:
        yaml.dump(spec, f)

    return spec

def mk_sqlite_db(path = cfg.PROJECTS, fname = IDX_FNAME,  cmds = TBL_CREATE_IDX_CMDS, *args):
    with sqlite3.connect(os.path.join(path, fname)) as conn: 
        for tcc in cmds:
            conn.execute(cmds[tcc])         
    conn.close() 

def fill_text_db():
    mk_sqlite_db(path = cfg.TEXTS, fname = TEXTS_FNAME, cmds = TEXTS_SELECT_CMDS)

def setup_scrape_tests():
    shutil.copyfile(os.path.join(cfg.TESTS, 'snopes.sqlite'), IDX_PTH)
    shutil.copyfile( os.path.join(cfg.TESTS, 'snopes_texts.sqlite'), TEXTS_PTH)
    shutil.copyfile( os.path.join(cfg.TESTS, 'snopes_clean.yaml'), CLNR_PTH)

    assert os.path.isfile(IDX_PTH)
    assert os.path.isfile(TEXTS_PTH)
    assert os.path.isfile(CLNR_PTH)

def create_web_resources(folder = TMP_WEB_DIR):
    suffix = '<p>There is a link to itself %s</p></body></html>' 
    absolute = '<p>There is a link to <a href="http://%s" > </p></body></html>' 
    relative = '<p>There is a link to <a href="%s" > </p></body></html>' 
 
    domain = URLS[0]

    pages = 'home.html page1.html page2.html'.split()
    links = 'page2.html home.html page1.html'.split()
    
    for pg, link in zip(pages, links):
        with open(os.path.join(folder, pg), 'w') as f:
            url = os.path.join(domain, pg)
        
            template = HTML_TEMPLATE[:-len(suffix)] + absolute

            f.write(template%(url,url, url, os.path.join(domain, link)))

class Application:
    def __call__(self, environ, start_response):

        start_response(
                '200 OK',
                [('Content-type', 'text/html; charset=utf-8')] )

        path_info = environ['PATH_INFO']
        server_name = environ['SERVER_NAME']
        server_port = environ['SERVER_PORT']
        server_host = environ['HTTP_HOST']
        request_method = environ['REQUEST_METHOD']

        pth = os.path.join(TMP_WEB_DIR, environ['PATH_INFO'][1:])
        with open(pth) as f:
            html = f.read()

        return [bytes(html, 'utf8')]

class MockVisitor():
    def __init__(self):
       self.app = TestApp(Application())

    def visit(self, url):

        if url.startswith('www'):
            #this is here to emulate urllib3 which seems to add the http scheme automatically
            url = 'http://' + url
        resp = self.app.get(url = url)

        setattr(resp, 'data', resp.text)

        return resp

class MockResponse: pass

def mock_response_200(url):
    r = MockResponse()
    if url.startswith('https://'):
        url = url[len('https://'):]
        
    r.data = MARKUPS[url]
    r.status = STATUS[url]

    return r

def mock_response_404(url):
    statuses = {}
    for i, u in enumerate(URLS): statuses[u] = 404

    r = MockResponse()
    r.data = MARKUPS[url]
    r.status = statuses[url]

    return r

def mock_visitor(side_effect = mock_response_200):
    resp = mock.Mock()
    resp.side_effect = side_effect

    m = mock.Mock()
    m.visit = resp
    return m

class MyObj: pass

class MockParser:
    '''
    lalala
    '''
    instanced_with = []
    def __init__(self, rules):
        self.__class__.instanced_with = [rules]
        self.rules = rules
        self.make_soup_called = False
        self.find_url_links_called = False
        self.find_title_called = False
        self.get_page_title_called = False

    def make_soup(self, html):
        self.soup = html
        self.make_soup_called = True

    def find_url_links(self):
        self.find_url_links_called = True
        if '<a' in self.soup:
            if 'href' in self.soup:
                s = self.soup.split('<a href="')[1]
                s = s.split('" ></p></body></html>')[0]
                s = s[len('https://'):]
                return [s]
            else:
                return []
        return []

    def get_page_title(self):
        self.get_page_title_called = True
        if 'title' in self.soup:
            i1 = self.soup.index('<title>')
            i2 = self.soup.index('</title>')
            return self.soup[i1:i2+8]
        return ''

def mock_index_pg(a, b, c, d): return True

def mock_index():
    ipg = mock.Mock()
    ipg.side_effect = mock_index_pg

    m = mock.Mock()
    m.index_page = ipg
    return m
