#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
wrapper for running app directly from source tree
"""
from app import main
      
if __name__ == '__main__':
    main.run_cli()
