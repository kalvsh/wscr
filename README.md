# wscr

wscr is the scraper used in wordbuzz project
- Scrapes natural language from websites one website at a time.
- Stores scraped text as json files in a directory.
- Keeps an index.sqlite for each website scraped
- It has two modes: 
 - create: creates a crawl project
 - crawl: crawls a portal


## Getting Started

```
$ git clone ...
$ pip install requirements.txt
```

## Usage
 	- go to root of project 
	- to create a scrape project:
		
		```
		$ python run.py create name-of-project-you-want-to-create
		```

	- to crawl a website

		```
		$ python run.py crawl name-of-project-you-created-to-crawl-website
		```

## Running the tests

TODO

## Built With

* [python3.8](http://www.python.org)


## Authors

* **C Kalvsh**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc


o
