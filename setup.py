#!/usr/bin/env python3
"""
Packaging for example CLI tool
"""
from setuptools import setup, find_packages

import app as package

def read_file(filename):
    """Fetch the contents of a file"""
    with open(filename) as file:
        return file.read()

from setuptools import setup, find_packages

setup(
    name=package.__name__.replace('_', '-'),
    version=package.__version__,
    description=package.__doc__.strip().split('\n')[0],
    long_description=read_file('README.md'),
    long_description_content_type='text/markdown',
    url=package.__url__,
    author=package.__author__,
    author_email=package.__email__,
    license=package.__license__,
    classifiers=[
        'Development Status :: Beta',
        'Topic :: Scraping ',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8', ],
    python_requires='>=3.6',
    packages=find_packages(exclude=['tests']),
    install_requires=read_file('requirements.txt'),
    entry_points={'console_scripts': ['wscr = app.main:run_cli', ], }, )
